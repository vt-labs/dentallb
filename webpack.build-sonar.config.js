const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");
const RemoveEmptyScriptsPlugin = require("webpack-remove-empty-scripts");

const transformLiquid = (content) => {
    const stringifiedContent = content.toString();
    return stringifiedContent.replace(/{%.*%}/g, "").replace(/{{.*?}}/g, "1");
};

module.exports = {
    mode: "development",
    output: {
        path: path.resolve(__dirname, "src"),
    },
    entry: {},
    plugins: [
        new CopyPlugin({
            patterns: [
                {
                    from: "./**/*(sections|assets|snippets|layout|templates)/**/*",
                    context: "./src",
                    transform: transformLiquid,
                    force: true,
                    noErrorOnMissing: true,
                },
            ],
        }),
        new RemoveEmptyScriptsPlugin(),
    ],
};
