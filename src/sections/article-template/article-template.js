const formatStringForId = (string) => string.toLowerCase().replace(/[^a-zA-Z]/g, "");

function isScrolledIntoView(element) {
    const elementCoordinates = element.getBoundingClientRect();
    const elemTop = elementCoordinates.top;
    const elemBottom = elementCoordinates.bottom;
    return elemTop >= 0 && elemBottom <= window.innerHeight;
}

const hideItems = (itemsList) => itemsList.length === 0 && $(".recent-stories").hide();

const recentList = $(".recent-stories-list").children("li"),
    recentListArray = [];

hideItems(recentList);

const currentArticleID = $("article").attr("data-article-id");

$(recentList).each(function () {
    const articleID = $(this).attr("data-article-id");
    /* if the current article ID matches anyone in the loop - then remove it */
    if (currentArticleID === articleID) {
        $(this).remove();
    } else {
        recentListArray.indexOf(articleID) === -1
            ? recentListArray.push(articleID)
            : $(this).remove();
    }
});

hideItems(recentListArray);

const addClass = (element, className) => {
    element.classList.add(className);
};
const removeClass = (element, className) => {
    element.classList.remove(className);
};
document.addEventListener("DOMContentLoaded", function () {
    const articleContentHole = document
        .querySelector(".article-container")
        .querySelector(".relative");
    const articleContent = articleContentHole.querySelector(".article-content");
    const h2List = articleContent.querySelectorAll("h2");
    const contentTableWrapper = document.querySelector("#contentTable");
    const tableContentEll = contentTableWrapper.querySelector("ul");
    const dropdownTableOfContent = contentTableWrapper.querySelector(".table-of-content-dropDown");
    for (const h2Item of h2List) {
        const innerText = h2Item.innerText;
        const formatedForClass = formatStringForId(innerText);
        h2Item.id = formatedForClass;
        const liEll = document.createElement("li");
        liEll.className = formatedForClass;
        const hrefEll = document.createElement("a");
        hrefEll.setAttribute("href", `#${formatedForClass}`);
        hrefEll.innerHTML = innerText;
        liEll.appendChild(hrefEll);
        tableContentEll.appendChild(liEll);

        const ellOnScreen = document.querySelector(`#${formatedForClass}`);
        const parrentLiEll = document.querySelector(`.${formatedForClass}`);
        window.addEventListener("scroll", function (e) {
            if (isScrolledIntoView(ellOnScreen)) {
                const siblings = [...parrentLiEll.parentElement.children];
                siblings.forEach((liItem) => liItem.classList.remove("highLighted"));
                parrentLiEll.classList.add("highLighted");
            }
        });
    }
    const commentsLi = document.createElement("li");
    const commentLink = document.createElement("a");
    commentLink.setAttribute("href", `#ask_question`);
    commentLink.innerHTML = "Comments";
    commentsLi.appendChild(commentLink);
    tableContentEll.appendChild(commentsLi);

    const liList = tableContentEll.querySelectorAll("li");

    for (let i = 0; i < liList.length; i++) {
        liList[i].addEventListener("click", function (e) {
            e.preventDefault();
            const linkHref = liList[i].querySelector("a").getAttribute("href");
            const scrollTarget = document.querySelector(linkHref);
            const topOffset = document.querySelector("header").offsetHeight;
            const elementPosition = scrollTarget.getBoundingClientRect().top;
            const offsetPosition = elementPosition - topOffset;
            window.scrollBy({
                top: offsetPosition,
                behavior: "smooth",
            });
            removeClass(contentTableWrapper, "active");
        });
    }

    dropdownTableOfContent.addEventListener("click", () => {
        if (contentTableWrapper.classList.contains("active")) {
            removeClass(contentTableWrapper, "active");
        } else {
            addClass(contentTableWrapper, "active");
        }
    });
});
