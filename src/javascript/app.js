import "./sass/styles.scss";

import * as scripts from "./js";
import CustomerDAO from "./js/customerDAO";
import LoggedSwitcher from "./js/switchersOptionsClasses/loggedRender";
import UnLoggedSwitcher from "./js/switchersOptionsClasses/unLoggedRenderer";
import MultiSubscriptions from "./js/switchersOptionsClasses/multiSubscriptions";
import Customer from "./js/customerClass";
import { createComponentRenderer } from "./js/reactProductPage/index";
import {
    getProductState,
    rendererLocator,
    updateProductUrl,
    updateOriginProductPage,
} from "globalUtils";
const { relevantProductLinks } = window;
const constructorParameters = {
    updateProductUrl,
    updateOriginProductPage,
    productState: relevantProductLinks && getProductState(),
    renderOptions: createComponentRenderer(rendererLocator.optionsBlock),
    renderTrialComponent: createComponentRenderer(rendererLocator.trialButton),
    renderOriginComponent: createComponentRenderer(rendererLocator.originButton),
    renderPaymentTermsInfoComponent: createComponentRenderer(rendererLocator.paymentTermsInfo),
    renderTitleComponent: createComponentRenderer(rendererLocator.productTitle),
    renderPriceComponent: createComponentRenderer(rendererLocator.productPrice),
    renderSubscriptionOptions: createComponentRenderer(rendererLocator.subscriptionOptions),
    renderSubscriptionComponent: createComponentRenderer(rendererLocator.subscriptionButton),
    renderAddToCartComponent: createComponentRenderer(rendererLocator.addToCartButton),
};
const isReactContainsTemplate = !!(
    constructorParameters.renderOriginComponent && relevantProductLinks
);
const customerDAO = new CustomerDAO().getCustomer();
const customerStatus = new Customer(customerDAO).getCustomerStatus();
const renderOptions = {
    loggedIn: () => new LoggedSwitcher(constructorParameters).render(),
    loggedOut: () => new UnLoggedSwitcher(constructorParameters).render(),
    anyCustomerStatus: () => new MultiSubscriptions(constructorParameters).render(),
};

const App = () => {
    scripts.preLoader();
    scripts.currentVariant();
    scripts.productDetails();
    scripts.additionalInfoSection();
    scripts.accountRepurchase();
    isReactContainsTemplate && renderOptions[customerStatus]();
};

export default App;
