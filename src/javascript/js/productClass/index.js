import { getProductOptions } from "globalServices";
import { formatPriceForLayout } from "globalUtils";

class Product {
    constructor({ product, activeVariant }) {
        this.product = product;
        this.activeVariant = activeVariant;
    }

    getOptions() {
        return getProductOptions(this.product);
    }

    getProductVariants() {
        return this.product.variants;
    }

    getVariantPrice() {
        const variantsList = this.getProductVariants();
        const chosenVariantId = this.activeVariant;
        const { price, compare_at_price } = variantsList.find(({ id }) => id === +chosenVariantId);
        const NUMBER_OF_DECIMALS = 0;
        const formattedPrice = formatPriceForLayout(price, NUMBER_OF_DECIMALS);
        const formattedCompareAtPrice = formatPriceForLayout(compare_at_price, NUMBER_OF_DECIMALS);
        return { price: formattedPrice, compareAtPrice: formattedCompareAtPrice };
    }

    getProductTitle() {
        const { title } = this.product;
        return { title };
    }
}

export default Product;
