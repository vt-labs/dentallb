import Product from "./index";

jest.mock("globalServices", () => ({
    getProductOptions: (variants) => variants,
}));
jest.mock("globalUtils", () => ({
    formatPriceForLayout: (price) => price,
}));

const TEST_PRODUCT = {
    title: "trialProductTitle",
    variants: [
        { id: 1398, price: "100", compare_at_price: null },
        { id: 13569, price: "100", compare_at_price: null },
    ],
};

describe("productClass test", () => {
    test("productClass test origin product", () => {
        const productClass = new Product({ product: TEST_PRODUCT, activeVariant: "1398" });
        expect(JSON.stringify(productClass.getOptions())).toBe(JSON.stringify(TEST_PRODUCT));
        expect(productClass.getProductVariants()).toBe(TEST_PRODUCT.variants);
        expect(JSON.stringify(productClass.getProductTitle())).toBe(
            JSON.stringify({
                title: TEST_PRODUCT.title,
            })
        );
        expect(JSON.stringify(productClass.getVariantPrice())).toBe(
            JSON.stringify({
                price: TEST_PRODUCT.variants[0].price,
                compareAtPrice: TEST_PRODUCT.variants[0].compare_at_price,
            })
        );
    });
});
