import { getChosenVariantId } from "globalServices";

class ProductDAO {
    getOriginProduct() {
        const { originalProduct: product } = window;
        const activeVariant = getChosenVariantId(product.variants);
        return { product, activeVariant };
    }

    getTrialProduct() {
        const { trialProduct: product } = window;
        const activeVariant = getChosenVariantId(product.variants);
        return { product, activeVariant };
    }

    getFirstSubscriptionProduct() {
        const { subscriptionProducts } = window;
        const product = subscriptionProducts[0];
        const activeVariant = getChosenVariantId(product.variants);
        return { product, activeVariant };
    }
}

export default ProductDAO;
