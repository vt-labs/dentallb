export interface ICartData {
    readonly lineItems: Array<number | string | boolean>;
    readonly itemsCount: number;
    readonly totalPrice: number;
    readonly totalDiscount: number;
    readonly originalTotalPrice: number;
}

export interface IAddToCartRequestItem {
    variantId: number;
    quantity: number;
    properties?: string;
    selling_plan?: string;
}
export interface IAddToCartRequestData {
    items: Array<IAddToCartRequestItem>;
}

export interface ICartDataService {
    updateItem(variantId: number, quantity: number): Promise<void>;
    addItem(obj: IAddToCartRequestData): Promise<void>;
    fetchItems(): Promise<ICartData>;
    getValues?(): Promise<ICartData>;
    changeItem(line: number, properties: string): Promise<void>;
}

export interface FetchedDataArg {
    items: Array<number | string | boolean>;
    item_count: number;
    total_price: number;
    total_discount: number;
    original_total_price: number;
}
export interface ICartItems {
    // eslint-disable-next-line @typescript-eslint/ban-types
    line_items: Array<{}>;
}
export interface ILineItemProperties {
    _source_placement: string;
}
export interface ILineItemShopify {
    discounted_price: number;
    discounts: Array<string>;
    final_line_price: number;
    final_price: number;
    gift_card: boolean;
    grams: number;
    handle: string;
    id: number;
    image: string;
    key: string;
    line_level_discount_allocations: Array<string>;
    line_level_total_discount: number;
    line_price: number;
    original_line_price: number;
    original_price: number;
    price: number;
    product_description: string;
    product_has_only_default_variant: boolean;
    product_id: number;
    product_title: string;
    product_type: string;
    properties: ILineItemProperties;
    quantity: number;
    requires_shipping: boolean;
    sku: string;
    taxable: boolean;
    title: string;
    total_discount: number;
    url: string;
    variant_id: number;
    variant_options: Array<string>;
    variant_title: string;
    vendor: string;
}
export interface ILineItems {
    items: Array<ILineItemShopify>;
}

export interface IAddToCartAnalytics {
    (data: ILineItems): void;
}
