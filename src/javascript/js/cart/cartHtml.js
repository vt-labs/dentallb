import { formatPriceForLayout } from "globalUtils";

export const emptyCartHTML = `<div class="text-holder d-flex      align-items-center justify-content-center">
<p>Your cart is empty</p>
</div>`;

export const lineItemHML = (item) => {
    const {
        url,
        image,
        product_title,
        quantity,
        id,
        variant_title,
        final_line_price,
        original_line_price,
    } = item;
    return `<li>
				<div class="cart-table-body flex">
						<div class="drawerImg text-center">
								<a href="${url}">
										<img class="img-fluid" src="${image}"
												alt="${product_title}"
										/>
								</a>
						</div>
						<div class="flex-column flex-space-between  				line-item-data-holder" 
												data-cart-item-quantity="${quantity}" 
												data-cart-item-variant-id="${id}">
								<div>
										<div>
												<h4 class="h3"><a href="${url}" aria-label="${product_title} page">${product_title}</a></h4>
										</div>
								</div>
								<div class="variant-title">
								${variant_title ?? ""}
								</div>
						<div>
								<div>
										<div class="table-item" data-head="Quantity">
								</div>
												<div class="input-group plus-minus-input cart-quantity-outer">
														<button type="button" class="minus-qty
														update-quantity"   data-quantity="minus" data-cy="line-item-quantity-minus"data-field="quantity-1"          aria-label="decrease quantity">
														</button>
																<input data-quantity="value"
																class="update-quantity-input input-group-field  cart__quantity-selector"  min="1"                       type="number"
																name="quantity-1"
																value="${quantity}"
																aria-label="quantity input"                     data-cy="line-item-quantity">
														<button type="button" class="plus-qty 
														update-quantity"                    data-quantity="plus"
														data-cy="line-item-quantity-plus" data-field="quantity-1" aria-label="increase quantity">
														</button>
												</div>
										</div>
										</div>
										</div>
										<div class="flex-justify-column-center line-item-data-holder" 
										data-cart-item-quantity="${quantity}" 
										data-cart-item-variant-id="${id}">
												<a href="#" aria-label="Remove ${product_title}" class="product-delete update-quantity"
												data-cy="line-item-remove" data-quantity="zero">
														<svg width="20px" height="21px" viewBox="0 0 20 21">
																<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
																		<g transform="translate(-949.000000, -161.000000)" stroke="#1B2061" stroke-width="2">
																				<g transform="translate(959.000068, 171.300003) rotate(-315.000000)         translate(-959.000068, -171.300003) translate(953.000000, 165.299935)">
																						<line x1="0" y1="6.00006823" x2="12.0001365" y2="6.00006823"></line>
																						<line x1="6.00006823" y1="0" x2="6.00006823" y2="12.0001365"></line>
																				</g>
																		</g>
																</g>
														</svg>
												</a>
										</div>
										</div>
										<div class="flex-justify-column-center text-right">
										${
                                            original_line_price !== final_line_price
                                                ? `<div class="product-price-holder original-price">
										${formatPriceForLayout(original_line_price)} 
										</div>`
                                                : ""
                                        }
										<div class="product-price-holder">${formatPriceForLayout(final_line_price)}</div>
										</div>
				</li>`;
};
