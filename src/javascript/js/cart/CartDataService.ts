import {
    ICartDataService,
    ICartData,
    FetchedDataArg,
    IAddToCartRequestData,
    IAddToCartRequestItem,
    IAddToCartAnalytics,
} from "./ICart";

interface Data {
    updates?: { [x: number]: number };
    items?: Array<IAddToCartRequestItem>;
    line?: number;
    properties?: string;
}

export class CartDataService implements ICartDataService {
    constructor(addToCartAnalytics: IAddToCartAnalytics) {
        this.addToCartAnalytics = addToCartAnalytics;
    }

    private addToCartAnalytics;

    private createPostRequestBody = (data: Data) => {
        return {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(data),
        };
    };

    private prepareData = (data: FetchedDataArg): ICartData => {
        const { items, item_count, total_price, total_discount, original_total_price } = data;
        return {
            lineItems: items,
            itemsCount: item_count,
            totalPrice: total_price,
            totalDiscount: total_discount,
            originalTotalPrice: original_total_price,
        };
    };

    updateItem = async (variantId: number, quantity: number): Promise<void> => {
        const data = { updates: { [variantId]: quantity } };
        await fetch("/cart/update.js", this.createPostRequestBody(data));
    };

    addItem = async (data: IAddToCartRequestData): Promise<void> => {
        await fetch("/cart/add.js", this.createPostRequestBody(data));
    };

    fetchItems = async (): Promise<ICartData> => {
        const response = await fetch("/cart.js");
        return this.prepareData(await response.json());
    };

    changeItem = async (line: number, properties: string): Promise<void> => {
        const data = { line, properties };
        await fetch("/cart/change.js", this.createPostRequestBody(data));
    };
}
