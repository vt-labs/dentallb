import { ICartDataService, ICartData, IAddToCartRequestData } from "./ICart";

export class Cart {
    constructor(cartDataService: ICartDataService) {
        this.cartDataService = cartDataService;
    }

    cartDataService: ICartDataService;

    getValues = async (): Promise<ICartData> => {
        const {
            lineItems,
            itemsCount,
            totalPrice,
            totalDiscount,
            originalTotalPrice,
        }: ICartData = await this.cartDataService.fetchItems();
        return {
            lineItems,
            itemsCount,
            totalPrice,
            totalDiscount,
            originalTotalPrice,
        };
    };

    addItem = (data: IAddToCartRequestData): Promise<void> => this.cartDataService.addItem(data);

    updateItem = (id: number, qty: number): Promise<void> =>
        this.cartDataService.updateItem(id, qty);

    changeItem = (line: number, properties: string): Promise<void> =>
        this.cartDataService.changeItem(line, properties);
}
