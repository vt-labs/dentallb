import SwitchersManager from "../switchersClass/index";

class LoggedSwitcher extends SwitchersManager {
    constructor({
        productState,
        renderOptions,
        updateProductUrl,
        renderTrialComponent,
        renderOriginComponent,
        updateOriginProductPage,
        renderPaymentTermsInfoComponent,
        renderTitleComponent,
        renderPriceComponent,
        renderSubscriptionComponent,
    }) {
        super({
            productState,
            renderOptions,
            updateProductUrl,
            renderTrialComponent,
            renderOriginComponent,
            updateOriginProductPage,
            renderPaymentTermsInfoComponent,
            renderTitleComponent,
            renderPriceComponent,
            renderSubscriptionComponent,
        });
        this.renderPrice = this.renderPrice.bind(this);
    }

    render() {
        const product = this.getProduct();
        this.renderOriginComponent({
            onClick: this.clickOriginSwitcher,
            isActive: this.getOriginSwitcherState(),
        });
        this.updateOriginProductPage(this.getOriginSwitcherState());
        this.renderOptions({
            productData: {
                options: product.getOptions(),
                variants: product.getProductVariants(),
            },
            getVariantPrice: this.renderPrice,
        });
        this.renderPaymentTermsInfoComponent();
        this.renderTitleComponent(product.getProductTitle());
        this.renderPriceComponent(product.getVariantPrice());
    }
}
export default LoggedSwitcher;
