import SwitchersManager from "../switchersClass/index";
import { Cart, CartDataService } from "../cart";
import { drawCartData } from "../cartInit";
import {
    getStoreQuantity,
    setStoreQuantity,
    getSellingPlanID,
    clearChosenVariantId,
} from "globalServices";
import ProductDAO from "../productDAO";
import Product from "../productClass";
import SubscriptionProductsClass from "../subscriptionProducts";

class MultiSubscriptions extends SwitchersManager {
    constructor({
        productState,
        renderOptions,
        updateProductUrl,
        renderTrialComponent,
        renderOriginComponent,
        updateOriginProductPage,
        renderPaymentTermsInfoComponent,
        renderTitleComponent,
        renderPriceComponent,
        renderAddToCartComponent,
        renderSubscriptionComponent,
        renderSubscriptionOptions,
    }) {
        super({
            productState,
            renderOptions,
            updateProductUrl,
            renderTrialComponent,
            renderOriginComponent,
            updateOriginProductPage,
            renderPaymentTermsInfoComponent,
            renderTitleComponent,
            renderPriceComponent,
            renderSubscriptionComponent,
        });
        this.renderSubscriptionComponent = renderSubscriptionComponent;
        this.renderSubscriptionOptions = renderSubscriptionOptions;
        this.renderAddToCartComponent = renderAddToCartComponent || null;
        this.renderPrice = this.renderPrice.bind(this);
        this.addToCart = this.addToCart.bind(this);
        this.renderPriceSubscriptionTemplate = this.renderPriceSubscriptionTemplate.bind(this);
        this.changeSubscriptionType = this.changeSubscriptionType.bind(this);
        this.manageSubscriptions = new SubscriptionProductsClass();
    }

    addToCart() {
        const cart = new Cart(new CartDataService());
        const { activeVariant, product } = this.getProductFromList();
        const selling_plan = getSellingPlanID(product);
        cart.addItem({
            items: [
                {
                    id: activeVariant,
                    quantity: getStoreQuantity(),
                    selling_plan,
                },
            ],
        }).then(() => {
            drawCartData();
        });
    }

    getSubscriptionTemplateProduct() {
        const productDAO = new ProductDAO();
        const activeProduct = this.isOrigin
            ? productDAO.getOriginProduct()
            : productDAO.getFirstSubscriptionProduct();
        return new Product(activeProduct);
    }

    renderPriceSubscriptionTemplate() {
        const product = this.getSubscriptionTemplateProduct();
        this.renderPriceComponent(product.getVariantPrice());
    }

    changeSubscriptionType(option) {
        clearChosenVariantId();
        const selectedSubscriptionProduct = this.manageSubscriptions.getSelectedSubscriptionProduct(
            option
        );
        const product = this.getSelectedSubscriptionProduct(selectedSubscriptionProduct);
        this.renderPriceComponent(product.getVariantPrice());
        this.renderSubscriptionOptions({
            productData: {
                options: product.getOptions(),
                variants: product.getProductVariants(),
            },
            isOrigin: this.isOrigin,
            subscriptionOptions: this.manageSubscriptions.getSubscriptionOptionList(),
            onChangeSubscriptionProduct: this.changeSubscriptionType,
        });
    }

    getSelectedSubscriptionProduct(product) {
        return new Product(product);
    }

    render() {
        const product = this.getProductFromList();
        this.updateProductUrl({ isOrigin: this.isOrigin });
        setStoreQuantity(1);
        this.renderSubscriptionComponent({
            onClick: this.clickTrialSwitcher,
            isActive: this.getTrialSwitcherState(),
            buttonContent: {
                buttonText: "Subscribe for this product",
                aboveBtnText: "See how subscription work",
            },
        });
        this.renderOriginComponent({
            onClick: this.clickOriginSwitcher,
            isActive: this.getOriginSwitcherState(),
        });
        this.renderAddToCartComponent({
            buttonText: this.getAddToCartBtnText(),
            onClick: this.addToCart,
        });
        this.renderOptions({
            productData: {
                options: product.getOptions(),
                variants: product.getProductVariants(),
            },
            getVariantPrice: this.renderPriceSubscriptionTemplate,
            isOrigin: this.isOrigin,
            isSubscriptionTemplate: true,
        });
        this.renderSubscriptionOptions({
            productData: {
                options: product.getOptions(),
                variants: product.getProductVariants(),
            },
            getVariantPrice: this.renderPriceSubscriptionTemplate,
            isOrigin: this.isOrigin,
            subscriptionOptions: this.manageSubscriptions.getSubscriptionOptionList(),
            onChangeSubscriptionProduct: this.changeSubscriptionType,
        });
        this.renderTitleComponent(product.getProductTitle());
        this.renderPriceComponent(product.getVariantPrice());
    }
}
export default MultiSubscriptions;
