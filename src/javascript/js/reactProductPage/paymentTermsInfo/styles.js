import styled from "styled-components";

export const PaymentTermsWrapperStyled = styled.div`
    color: #000000;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.4;
`;
