import React from "react";

import { PaymentTermsWrapperStyled } from "./styles";

const PaymentTermsInfo = () => (
    <PaymentTermsWrapperStyled
        className="payment-terms"
        dangerouslySetInnerHTML={{ __html: window.paymentTermsString }}
    />
);

export default PaymentTermsInfo;
