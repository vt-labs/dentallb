import React from "react";
import renderer from "react-test-renderer";

import PaymentTermsInfo from "../paymentTermsInfo";

describe("PaymentTermsInfo", () => {
    it("renders correctly", () => {
        window.paymentTermsString = `<shopify-payment-terms variant-id="31304288239669">
						Pay in 4 interest-free installments of $31.25 with Learn more
					</shopify-payment-terms>`;

        const tree = renderer.create(<PaymentTermsInfo />).toJSON();
        expect(tree).toMatchSnapshot();
    });
});
