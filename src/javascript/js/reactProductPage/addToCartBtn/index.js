import React from "react";

import Button from "Ui/button";

const AddToCartBtn = ({ onClick, isActive, buttonText }) => (
    <Button onClick={onClick} isActive={isActive}>
        <a>{buttonText}</a>
    </Button>
);

export default AddToCartBtn;
