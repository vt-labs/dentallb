import React, { useState, useEffect } from "react";

import Select from "Ui/select";
import { ErrorMessageStyled, SelectWrapper } from "./styled";

const QuantityQuestionSelector = ({
    data: { optionVariants, popupState, updateOptions, productChanged },
}) => {
    const [selected, setSelected] = useState(optionVariants[0]);
    const optionSelect = (e) => {
        setSelected(e.target.value);
        updateOptions(optionVariants, e.target.value);
    };
    const optionsData = {
        value: selected,
        onChange: optionSelect,
        optionVariants,
        isSelectHighlight: popupState && selected === optionVariants[0],
    };
    return (
        <SelectWrapper>
            <Select {...optionsData}></Select>
            {optionsData.isSelectHighlight && (
                <ErrorMessageStyled>Please select one</ErrorMessageStyled>
            )}
        </SelectWrapper>
    );
};
export default QuantityQuestionSelector;
