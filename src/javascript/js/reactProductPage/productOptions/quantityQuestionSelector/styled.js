import styled from "styled-components";

export const SelectWrapper = styled.div`
    position: relative;
`;
export const ErrorMessageStyled = styled.span`
    position: absolute;
    color: red;
    font-size: 12px;
    left: 0;
    bottom: -15px;
`;
