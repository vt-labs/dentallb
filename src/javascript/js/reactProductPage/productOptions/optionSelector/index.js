import React, { useState, useEffect } from "react";

import Select from "Ui/select";
import { getVariantId } from "globalUtils";
import { ErrorMessageStyled, SelectWrapper } from "./styled";

const OptionSelector = ({
    data: { optionVariants, variants, getVariantPrice, updateOptions, popupState, productChanged },
}) => {
    const [selected, setSelected] = useState(optionVariants[0]);
    useEffect(() => {
        setSelected(optionVariants[0]);
        updateOptions(optionVariants, selected);
    }, [productChanged]);
    useEffect(() => {
        getVariantId(variants);
    }, [variants]);
    const optionSelect = (e) => {
        setSelected(e.target.value);
        getVariantId(variants);
        getVariantPrice && getVariantPrice();
        updateOptions(optionVariants, e.target.value);
    };
    const optionsData = {
        value: selected,
        onChange: optionSelect,
        optionVariants,
        isSelectHighlight: popupState && selected === optionVariants[0],
    };
    return (
        <SelectWrapper>
            <Select {...optionsData}></Select>
            {optionsData.isSelectHighlight && (
                <ErrorMessageStyled>Please select one</ErrorMessageStyled>
            )}
        </SelectWrapper>
    );
};
export default OptionSelector;
