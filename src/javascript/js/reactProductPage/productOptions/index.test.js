import React from "react";
import { act } from "react-dom/test-utils";
import ReactDom from "react-dom";

import ProductOptions from ".";

let container = null;

beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    ReactDom.unmountComponentAtNode(container);
    container.remove();
    container = null;
});

jest.mock("./optionSelector", () => jest.fn(() => <div className="option-selector"></div>));
jest.mock("globalUtils", () => ({
    manageNextButtons: jest.fn(),
    optionsValidation: jest.fn(),
}));

const TEST_PRODUCT_DATA = {
    options: [
        {
            optionName: "optionName1",
        },
        {
            optionName: "optionName2",
        },
    ],
};

describe("TrialProductSwitcher", () => {
    test("TrialProductSwitcher test modal content open", () => {
        act(() => {
            ReactDom.render(
                <ProductOptions
                    productData={TEST_PRODUCT_DATA}
                    isOrigin={true}
                    isSubscriptionTemplate={false}
                />,
                container
            );
            const selectorsList = document.querySelectorAll(".selector-wrapper-custom");
            expect(selectorsList.length).toBe(TEST_PRODUCT_DATA.options.length);
        });
    });
    test("TrialProductSwitcher test modal content non render option", () => {
        act(() => {
            ReactDom.render(
                <ProductOptions
                    productData={TEST_PRODUCT_DATA}
                    isOrigin={false}
                    isSubscriptionTemplate={true}
                />,
                container
            );
            const selectorsList = document.querySelectorAll(".selector-wrapper-custom");
            expect(selectorsList.length).toBe(0);
        });
    });
});
