import React, { useEffect, useState } from "react";

import OptionSelector from "./optionSelector";
import QuantityQuestionSelector from "./quantityQuestionSelector";
import { OptionsWrapperStyled, CustomSelectorStyled, QuantityQuestionSelectorStyled } from "./styled";
import QuantityBlock from "../quantityBlock";
import { manageNextButtons, optionsValidation } from "globalUtils";

const ProductOptions = ({
    productData: { options, variants },
    getVariantPrice,
    isOrigin,
    isSubscriptionTemplate,
}) => {
    const [quantity, setQuantity] = useState(1);
    const [selectedOptions, setSelectedOptions] = useState([]);
    const [selectedAnswerOptions, setSelecteAnswerdOptions] = useState([]);
    const [popupState, setPopupState] = useState(false);
    const [productChanged, setProductChanged] = useState(false);
    useEffect(() => {
        setProductChanged(!productChanged);
        setSelectedOptions([]);
        manageNextButtons(optionsValidation(selectedOptions, options), setPopupState);
    }, [variants]);
    useEffect(() => {
        const hasAnswer = quantity > 1 && (selectedAnswerOptions.length !== 0 && !selectedAnswerOptions.includes("None selected"));
        if (hasAnswer) {
            localStorage.setItem("multiplePeopleAnswer", selectedAnswerOptions[0]);
        } else {
            localStorage.removeItem("multiplePeopleAnswer");
        }
        const validated = optionsValidation(selectedOptions, options) && 
                    !(quantity > 1 && (selectedAnswerOptions.length === 0 || selectedAnswerOptions.includes("None selected")));
        manageNextButtons(validated, setPopupState);
    }, [selectedOptions, selectedAnswerOptions]);
    if (isSubscriptionTemplate && !isOrigin) {
        return null;
    }
    const isJustified = options.length > 1;
    const updateOptions = (relativeOptionsList, selectedOption) => {
        const filteredSelected = selectedOptions.filter(
            (item) => !relativeOptionsList.includes(item)
        );
        setSelectedOptions([...filteredSelected, selectedOption]);
    };
    const updateAnswerOptions = (relativeOptionsList, selectedAnswerOption) => {
        const filteredSelected = selectedAnswerOptions.filter(
            (item) => !relativeOptionsList.includes(item)
        );
        setSelecteAnswerdOptions([...filteredSelected, selectedAnswerOption]);
    };
    return (
        <>
            <OptionsWrapperStyled isJustified={isJustified}>
                {options.map(({ optionName, optionVariants }, index) => {
                    const optionsWithFirstDefault = [].concat("None selected", optionVariants);
                    return (
                        <CustomSelectorStyled
                            className="selector-wrapper-custom product-options"
                            key={optionName + index}
                        >
                            <label>{optionName}</label>
                            <OptionSelector
                                data={{
                                    optionVariants: optionsWithFirstDefault,
                                    variants,
                                    getVariantPrice,
                                    updateOptions,
                                    popupState,
                                    productChanged,
                                }}
                            />
                        </CustomSelectorStyled>
                    );
                })}
                <QuantityBlock 
                    data={{
                        setQuantity: setQuantity,
                        quantity,
                        setSelecteAnswerdOptions: setSelecteAnswerdOptions,
                    }}
                />
            </OptionsWrapperStyled>
            {quantity > 1 && (<QuantityQuestionSelectorStyled
                className="selector-wrapper-custom"
            >
                <label>Is this order for more than one person?</label>
                <QuantityQuestionSelector
                    data={{
                        optionVariants: ["None selected", "Yes", "No"],
                        popupState,
                        updateOptions: updateAnswerOptions,
                        productChanged,
                    }}
                >
                </QuantityQuestionSelector>
            </QuantityQuestionSelectorStyled>)}
        </>
    );
};
export default ProductOptions;
