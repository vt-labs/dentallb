import styled from "styled-components";

export const OptionsWrapperStyled = styled.div`
    display: flex;
    ${({ isJustified }) => isJustified && "justify-content: space-between;"}
    align-items: end;
    > div:not(:first-child) {
        margin-left: 15px;
    }
`;

export const CustomSelectorStyled = styled.div`
    margin-bottom: 0px !important;
    min-width: 100px;
`;

export const QuantityQuestionSelectorStyled = styled.div`
    margin-bottom: 0px !important;
    min-width: 100px;
    margin-top: 20px;
`;
