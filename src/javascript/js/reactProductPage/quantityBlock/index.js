import React, { useState } from "react";

import { setStoreQuantity } from "globalServices";

const QuantityBlock = ({data: {setQuantity, quantity, setSelecteAnswerdOptions}}) => {
    const changeQuantity = (updatedQuantity) => {
        if (updatedQuantity < 1) {
            return;
        }
        setQuantity(updatedQuantity);
        setStoreQuantity(updatedQuantity);
        setSelecteAnswerdOptions([]);
    };
    return (
        <div className="js-product-single__quantity">
            <label>Quantity</label>
            <div className="quantity-outer">
                <div className="minus-qty" onClick={() => changeQuantity(quantity - 1)}></div>
                <div className="quantity-selector js-quantity-selector">{quantity}</div>
                <div className="plus-qty" onClick={() => changeQuantity(quantity + 1)}></div>
            </div>
        </div>
    );
};
export default QuantityBlock;
