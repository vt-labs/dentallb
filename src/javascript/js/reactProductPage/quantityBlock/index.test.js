import React from "react";
import TestUtils, { act } from "react-dom/test-utils";
import ReactDom from "react-dom";

import QuantityBlock from ".";

let container = null;

beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    ReactDom.unmountComponentAtNode(container);
    container.remove();
    container = null;
});

describe("Quantity block", () => {
    test("Quantity block test modal content open", () => {
        act(() => {
            ReactDom.render(<QuantityBlock />, container);
            const valueSection = document.querySelector(".quantity-selector");
            const plusBnt = document.querySelector(".plus-qty");
            expect(valueSection.innerHTML).toBe("1");
            TestUtils.Simulate.click(plusBnt);
        });
        act(() => {
            const valueSection = document.querySelector(".quantity-selector");
            expect(valueSection.innerHTML).toBe("2");
            const minusBnt = document.querySelector(".minus-qty");
            TestUtils.Simulate.click(minusBnt);
        });
        act(() => {
            const valueSection = document.querySelector(".quantity-selector");
            expect(valueSection.innerHTML).toBe("1");
            const minusBnt = document.querySelector(".minus-qty");
            TestUtils.Simulate.click(minusBnt);
        });
        act(() => {
            const valueSection = document.querySelector(".quantity-selector");
            expect(valueSection.innerHTML).toBe("1");
        });
    });
});
