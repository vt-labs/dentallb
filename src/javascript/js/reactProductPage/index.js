import React from "react";
import ReactDOM from "react-dom/client";

export const createComponentRenderer = ({ rootSelector, Component }) => {
    const rootElement = document.querySelector(rootSelector);
    if (!rootElement) {
        return null;
    }
    const root = ReactDOM.createRoot(rootElement);
    return (props) => {
        const element = <Component {...props} />;
        root.render(element);
    };
};
