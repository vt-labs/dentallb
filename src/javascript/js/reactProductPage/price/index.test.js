import React from "react";
import { act } from "react-dom/test-utils";
import ReactDom from "react-dom";

import Price from ".";

let container = null;

beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    ReactDom.unmountComponentAtNode(container);
    container.remove();
    container = null;
});

const TEST_PRICE_DATA = {
    price: "125",
    compareAtPrice: "100",
};
const TEST_PRICE_DATA_WITH_ZERO = {
    price: "125",
    compareAtPrice: "0",
};

describe("Price", () => {
    test("PriceComponent test with both price", () => {
        act(() => {
            ReactDom.render(<Price {...TEST_PRICE_DATA} />, container);
            const mainPriceElement = document.querySelector(".product-single__price").innerHTML;
            const comparePriceElement = document.querySelector(".product-single__sale-price")
                .innerHTML;
            expect(mainPriceElement).toBe(`$${TEST_PRICE_DATA.price}`);
            expect(comparePriceElement).toBe(`$${TEST_PRICE_DATA.compareAtPrice}`);
        });
    });
    test("PriceComponent test with single price", () => {
        act(() => {
            ReactDom.render(<Price {...TEST_PRICE_DATA_WITH_ZERO} />, container);
            const comparePriceElement = document.querySelector(".product-single__sale-price");
            expect(comparePriceElement).toBeNull();
        });
    });
});
