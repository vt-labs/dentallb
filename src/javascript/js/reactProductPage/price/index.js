import React from "react";

const Price = ({ price, compareAtPrice }) => {
    return (
        <div className="product-single__prices">
            <span className="product-single__price">${price}</span>
            {!!+compareAtPrice && <s className="product-single__sale-price">${compareAtPrice}</s>}
        </div>
    );
};

export default Price;
