import React from "react";
import { act } from "react-dom/test-utils";
import ReactDom from "react-dom";

import SelectOptionButton from ".";

let container = null;

beforeEach(() => {
    container = document.createElement("body");
    document.body.appendChild(container);
});

afterEach(() => {
    ReactDom.unmountComponentAtNode(container);
    container.remove();
    container = null;
});

const TEST_IS_ACTIVE = true;
const TEST_IS_NOT_ACTIVE = false;

describe("SelectOptionButton", () => {
    test("SelectOptionButton render with true state", () => {
        act(() => {
            ReactDom.render(<SelectOptionButton isActive={TEST_IS_ACTIVE} />, container);
            const wrapperElem = document.querySelector("div");
            const borderValue = window.getComputedStyle(wrapperElem).border;
            expect(borderValue).toBe("2px solid #f07826");
        });
    });
    test("SelectOptionButton render with false state", () => {
        act(() => {
            ReactDom.render(<SelectOptionButton isActive={TEST_IS_NOT_ACTIVE} />, container);
            const wrapperElem = document.querySelector("div");
            const borderValue = window.getComputedStyle(wrapperElem).border;
            expect(borderValue).toBe("2px solid #ffffff");
        });
    });
});
