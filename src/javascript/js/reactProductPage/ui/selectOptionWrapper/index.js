import React from "react";

import { ButtonWrapper } from "./styled";

const SelectOptionButton = ({ onClick, isActive, children }) => {
    return (
        <ButtonWrapper isActive={isActive} onClick={onClick}>
            {children}
        </ButtonWrapper>
    );
};

export default SelectOptionButton;
