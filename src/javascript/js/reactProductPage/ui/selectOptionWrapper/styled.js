import styled from "styled-components";

export const ButtonWrapper = styled.div`
    width: 100%;
    position: relative;
    border-radius: 2px;
    box-shadow: 0 0 8px 0 rgba(54, 54, 55, 0.24);
    margin-bottom: 20px;
    h4 {
        font-size: 14px;
        line-height: 1.4;
        font-weight: bold;
        margin-bottom: 4px;
    }
    a {
        margin-bottom: 0px;
    }
    ${({ isActive }) =>
        isActive
            ? `
		padding: 18px 20px 20px;
		border: solid 2px #f07826;
		&::before {
			width: 24px;
			height: 24px;
			position: absolute;
			right: -1px;
			top: -1px;
			content: "";
			background: url('{{ "icon-tick-with-bg.png"|asset_url}}') no-repeat top
				right / 24px !important;
		}
		p {
			font-size: 13px;
			line-height: 1.31;
			margin-bottom: 4px;
		}`
            : `
			padding: 15px 20px 14px;
			border: solid 2px #ffffff;
			cursor: pointer;
			&.trial{
				cursor: default;
			}
			`}
`;
