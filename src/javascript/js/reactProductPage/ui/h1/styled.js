import styled from "styled-components";

export const H1Styled = styled.h1`
    max-width: 420px;
    font-size: 30px;
    line-height: 1.2;
    margin-bottom: 18px;
    display: inline-block;
`;
