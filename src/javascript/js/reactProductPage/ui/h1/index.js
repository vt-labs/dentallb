import React from "react";

import { H1Styled } from "./styled";

const H1 = ({ children }) => <H1Styled>{children}</H1Styled>;

export default H1;
