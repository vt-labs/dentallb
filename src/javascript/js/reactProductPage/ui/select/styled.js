import styled from "styled-components";

export const SelectStyled = styled.select`
    ${({ isSelectHighlight }) => isSelectHighlight && " border: 2px solid red !important;"}
`;
