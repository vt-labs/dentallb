import React from "react";
import { SelectStyled } from "./styled";

const Select = ({ optionVariants, onChange, value, isSelectHighlight }) => {
    const optionsItems = optionVariants.map((optionItem, index) => (
        <option key={optionItem + index} value={optionItem}>
            {optionItem}
        </option>
    ));
    return (
        <SelectStyled
            isSelectHighlight={isSelectHighlight}
            className="single-option-selector-react"
            value={value}
            onChange={onChange}
        >
            {optionsItems}
        </SelectStyled>
    );
};
export default Select;
