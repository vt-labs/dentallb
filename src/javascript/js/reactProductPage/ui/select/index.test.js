import React from "react";
import { act } from "react-dom/test-utils";
import ReactDom from "react-dom";

import Select from ".";

let container = null;

beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    ReactDom.unmountComponentAtNode(container);
    container.remove();
    container = null;
});

jest.mock("globalUtils", () => ({
    getVariantId: (variants) => variants[0],
}));

const TEST_DATA = {
    optionVariants: ["optionItem1", "optionItem2"],
    onChange: jest.fn(),
    value: "value",
};

describe("OptionSelector", () => {
    test("test change select value", () => {
        act(() => {
            ReactDom.render(<Select {...TEST_DATA} />, container);
            const optionsList = document.querySelectorAll("option");
            expect(optionsList.length).toBe(TEST_DATA.optionVariants.length);
            const selectElement = document.querySelector("select");
            const selectedValue = selectElement.value;
            expect(selectedValue).toBe(TEST_DATA.optionVariants[0]);
            optionsList[1].selected = "selected";
            const selectedValueAfterChange = selectElement.value;
            expect(selectedValueAfterChange).toBe(TEST_DATA.optionVariants[1]);
        });
    });
});
