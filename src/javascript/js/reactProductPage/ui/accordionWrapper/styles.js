import styled from "styled-components";

export const AccordionBlockStyled = styled.div`
    visibility: hidden;
    padding: 0px 30px 0px 30px;
    height: 0;
    overflow: hidden;
    opacity: 0;
    transition: all 0.01s ease-out;
    ${({ isOpened }) =>
        isOpened &&
        ` 
		visibility: visible;
		padding: 0px 20px 10px 20px;
		height:auto;
		overflow:none;
		opacity:1;`}
`;
