import React from "react";

import { AccordionBlockStyled } from "./styles";

const Accordion = ({ isOpened, children }) => (
    <AccordionBlockStyled isOpened={isOpened}>{children}</AccordionBlockStyled>
);

export default Accordion;
