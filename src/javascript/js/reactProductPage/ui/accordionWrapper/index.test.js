import React from "react";
import { act } from "react-dom/test-utils";
import ReactDom from "react-dom";

import Accordion from ".";

let container = null;

beforeEach(() => {
    container = document.createElement("body");
    document.body.appendChild(container);
});

afterEach(() => {
    ReactDom.unmountComponentAtNode(container);
    container.remove();
    container = null;
});

const IS_OPENED_TRUE = true;
const IS_OPENED_FALSE = false;

describe("Accordion", () => {
    test("Accordion render with true state", () => {
        act(() => {
            ReactDom.render(<Accordion isOpened={IS_OPENED_TRUE} />, container);
            const wrapperElem = document.querySelector("div");
            const visibilityValue = window.getComputedStyle(wrapperElem).visibility;
            expect(visibilityValue).toBe("visible");
        });
    });
    test("Accordion render with false state", () => {
        act(() => {
            ReactDom.render(<Accordion isOpened={IS_OPENED_FALSE} />, container);
            const wrapperElem = document.querySelector("div");
            const visibilityValue = window.getComputedStyle(wrapperElem).visibility;
            expect(visibilityValue).toBe("hidden");
        });
    });
});
