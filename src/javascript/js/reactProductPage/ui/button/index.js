import React from "react";

import { Btn } from "./styled";

const Button = ({ onClick, children }) => <Btn onClick={onClick}>{children}</Btn>;

export default Button;
