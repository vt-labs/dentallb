import styled from "styled-components";

export const Btn = styled.div`
    cursor: pointer;
    display: inline-block;
    font-size: 14px;
    font-weight: 700;
    text-align: center;
    width: 100%;
    letter-spacing: normal;
    text-transform: capitalize;
    border-radius: 2px;
    border: 1px solid;
    -webkit-appearance: none;
    -moz-appearance: none;
    background-color: #f07826;
    border-color: #f07826;
    padding: 16px 0;
    color: #fff;
    a {
        color: #fff;
    }
    :hover {
        background-color: #fff;
        a {
            color: #f07826;
        }
    }
`;
