import React from "react";
import { act } from "react-dom/test-utils";
import ReactDom from "react-dom";

import Button from ".";

let container = null;

beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    ReactDom.unmountComponentAtNode(container);
    container.remove();
    container = null;
});

const TEST_DATA = {
    children: jest.fn(() => <div className="child-div"></div>),
    onClick: jest.fn(),
};

describe("OptionSelector", () => {
    test("test change select value", () => {
        act(() => {
            ReactDom.render(
                <Button {...TEST_DATA}>
                    <div className="child-div"></div>
                </Button>,
                container
            );
            const childNode = document.querySelector(".child-div");
            expect(childNode.length).not.toBeNull();
        });
    });
});
