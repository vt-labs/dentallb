import React from "react";
import renderer from "react-test-renderer";

import OriginProductSwitcher from "./";

describe("OriginProductSwitcher", () => {
    const TEST_SWITCHER_MANAGER = {
        clickOriginSwitcher: jest.fn(),
        isActive: true,
    };
    jest.mock("Ui/selectOptionWrapper", () => jest.fn(() => <div classList="selectWrapper"></div>));
    it("renders correctly", () => {
        const tree = renderer
            .create(
                <>
                    <OriginProductSwitcher {...TEST_SWITCHER_MANAGER} />
                </>
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});
