import React from "react";

import SelectOptionButton from "Ui/selectOptionWrapper";

const OriginProductSwitcher = ({ onClick, isActive }) => {
    return (
        <>
            <SelectOptionButton onClick={onClick} isActive={isActive}>
                <h4>Let me buy it now</h4>
            </SelectOptionButton>
        </>
    );
};

export default OriginProductSwitcher;
