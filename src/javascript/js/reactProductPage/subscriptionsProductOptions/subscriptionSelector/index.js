import React, { useState } from "react";

import Select from "Ui/select";

const SubscriptionSelector = ({
    data: {
        subscriptionOptions,
        productData: { variants },
        onChangeSubscriptionProduct,
    },
}) => {
    const [selected, setSelected] = useState(subscriptionOptions[0]);
    const optionSelect = (e) => {
        setSelected(e.target.value);
        onChangeSubscriptionProduct(e.target.value);
    };

    const optionsData = {
        value: selected,
        onChange: optionSelect,
        optionVariants: subscriptionOptions,
    };
    return <Select {...optionsData} />;
};
export default SubscriptionSelector;
