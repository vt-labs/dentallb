import React, { useState, useEffect } from "react";

import Select from "Ui/select";
import { getVariantId } from "globalUtils";

const OptionSelector = ({ data: { optionVariants, variants, getVariantPrice } }) => {
    const [selected, setSelected] = useState(optionVariants[0].optionItem);
    useEffect(() => {
        getVariantId(variants);
    }, [variants]);
    const optionSelect = (e) => {
        setSelected(e.target.value);
        getVariantId(variants);
        getVariantPrice();
    };

    const optionsData = {
        value: selected,
        onChange: optionSelect,
        optionVariants,
    };
    return <Select {...optionsData} />;
};
export default OptionSelector;
