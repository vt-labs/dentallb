import React, { useEffect, useState } from "react";

import OptionSelector from "../productOptions/optionSelector";
import { OptionsWrapperStyled, CustomSelectorStyled } from "./styled";
import SubscriptionSelector from "./subscriptionSelector";
import { manageNextButtons, optionsValidation } from "globalUtils";

const SubscriptionProductOptions = ({
    productData,
    getVariantPrice,
    isOrigin,
    subscriptionOptions,
    onChangeSubscriptionProduct,
}) => {
    const [selectedOptions, setSelectedOptions] = useState([]);
    const [popupState, setPopupState] = useState(false);
    useEffect(() => {
        setSelectedOptions([]);
    }, [productData?.options]);
    useEffect(() => {
        manageNextButtons(optionsValidation(selectedOptions, productData?.options), setPopupState);
    }, [selectedOptions]);
    if (isOrigin) {
        return null;
    }
    const { options, variants } = productData;
    const isJustified = options.length > 1;
    const updateOptions = (relativeOptionsList, selectedOption) => {
        const filteredSelected = selectedOptions.filter(
            (item) => !relativeOptionsList.includes(item)
        );
        setSelectedOptions([...filteredSelected, selectedOption]);
    };
    return (
        <>
            <OptionsWrapperStyled isJustified={isJustified}>
                {options.map(({ optionName, optionVariants }, index) => {
                    const optionsWithFirstDefault = [].concat("None selected", optionVariants);
                    return (
                        <CustomSelectorStyled
                            className="selector-wrapper-custom product-options"
                            key={optionName + index}
                        >
                            <label>{optionName}</label>
                            <OptionSelector
                                data={{
                                    optionVariants: optionsWithFirstDefault,
                                    variants,
                                    getVariantPrice,
                                    updateOptions,
                                    popupState,
                                }}
                            />
                        </CustomSelectorStyled>
                    );
                })}
                <CustomSelectorStyled className="selector-wrapper-custom">
                    <label>Subscription option</label>
                    <SubscriptionSelector
                        data={{ subscriptionOptions, productData, onChangeSubscriptionProduct }}
                    />
                </CustomSelectorStyled>
            </OptionsWrapperStyled>
        </>
    );
};
export default SubscriptionProductOptions;
