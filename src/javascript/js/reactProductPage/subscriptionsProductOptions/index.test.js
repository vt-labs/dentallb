import React from "react";
import { act } from "react-dom/test-utils";
import ReactDom from "react-dom";

import SubscriptionProductOptions from ".";

let container = null;

beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    ReactDom.unmountComponentAtNode(container);
    container.remove();
    container = null;
});

jest.mock("../productOptions/optionSelector", () =>
    jest.fn(() => <div className="option-selector"></div>)
);
jest.mock("./subscriptionSelector", () =>
    jest.fn(() => <div className="subscription-selector"></div>)
);
jest.mock("globalUtils", () => ({
    manageNextButtons: jest.fn(),
    optionsValidation: jest.fn(),
}));

const TEST_PRODUCT_DATA = {
    options: [
        {
            optionName: "optionName1",
        },
        {
            optionName: "optionName2",
        },
    ],
};

describe("SubscriptionProductOptions", () => {
    test("SubscriptionProductOptions test modal content open", () => {
        act(() => {
            ReactDom.render(
                <SubscriptionProductOptions productData={TEST_PRODUCT_DATA} isOrigin={false} />,
                container
            );
            const selectorsList = document.querySelectorAll(".selector-wrapper-custom");
            expect(selectorsList.length).toBe(TEST_PRODUCT_DATA.options.length + 1);
        });
    });
    test("SubscriptionProductOptions test modal content non render option", () => {
        act(() => {
            ReactDom.render(<SubscriptionProductOptions isOrigin={true} />, container);
            const selectorNode = document.querySelector(".selector-wrapper-custom");
            expect(selectorNode).toBeNull();
        });
    });
});
