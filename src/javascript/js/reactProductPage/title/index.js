import React from "react";

import H1 from "Ui/h1";

const Title = ({ title }) => <H1>{title}</H1>;

export default Title;
