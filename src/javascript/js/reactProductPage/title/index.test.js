import React from "react";
import { act } from "react-dom/test-utils";
import ReactDom from "react-dom";

import Title from ".";

let container = null;

beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    ReactDom.unmountComponentAtNode(container);
    container.remove();
    container = null;
});

const TEST_TITLE = {
    title: "Any title",
};

describe("Title", () => {
    test("TitleComponent test with both price", () => {
        act(() => {
            ReactDom.render(<Title {...TEST_TITLE} />, container);
            const titleElementInnerHTML = document.querySelector("h1").innerHTML;
            expect(titleElementInnerHTML).toBe(TEST_TITLE.title);
        });
    });
});
