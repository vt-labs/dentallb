import React from "react";
import renderer from "react-test-renderer";
import ReactDom from "react-dom";

import { TrialAccordionContent } from "./index";

let container = null;

beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    ReactDom.unmountComponentAtNode(container);
    container.remove();
    container = null;
});

const accordionContent = { mainText: "mainText", underText: "underText" };

describe("TrialAccordionContent section", () => {
    it("renders correctly with trial content", () => {
        const tree = renderer
            .create(
                <>
                    <TrialAccordionContent accordionContent={accordionContent} />
                </>
            )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});
