import styled from "styled-components";

export const TrialDetailsListStyled = styled.ul`
    margin-bottom: 10px;
    li {
        font-size: 14px;
        line-height: 1.4;
        font-weight: 400;
    }
`;
export const TrialInfoStyled = styled.p`
    font-weight: 400;
    color: #f07826;
    margin-bottom: 10px;
`;
