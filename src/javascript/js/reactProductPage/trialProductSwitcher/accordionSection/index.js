import React from "react";

import { TrialDetailsListStyled, TrialInfoStyled } from "./styled";

export const TrialAccordionContent = ({ accordionContent: { mainText, underText } }) => (
    <>
        <TrialDetailsListStyled
            dangerouslySetInnerHTML={{
                __html: mainText,
            }}
        ></TrialDetailsListStyled>
        <TrialInfoStyled>{underText}</TrialInfoStyled>
    </>
);
