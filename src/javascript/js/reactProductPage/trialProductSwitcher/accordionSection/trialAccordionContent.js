export const accordionContentOptions = (isSubscriptionProduct) => {
    const trialAccordionContent = `
<li>Step 1. We will mail you the impression kit and you make your impression</li>
<li>Step 2. Return your impression and receive your custom night guard</li>
<li>Step 3. Try your night guard for a few nights</li>
<li>
Step 4. If you love it, keep it and we’ll process the remaining balance*;
otherwise, we offer free adjustments and/or replacement until you are 100% happy
with the guard. You can also opt for a full refund per our 365 satisfaction
guarantee.
</li>
`;
    const underTextInfo = `*the remaining balance will be processed in 28 days from the date your trial order
is placed.`;
    const subscriptionAccordionContent = `
<li>Step 1. Subscription description</li>
<li>Step 2. Subscription description</li>
<li>Step 3. Subscription description</li>
<li>
Step 4. Subscription description.
</li>
`;
    const subscriptionUnderTextInfo = `* subscriptionAccordionContent.`;
    return !isSubscriptionProduct
        ? {
              mainText: trialAccordionContent,
              underText: underTextInfo,
          }
        : {
              mainText: subscriptionAccordionContent,
              underText: subscriptionUnderTextInfo,
          };
};
