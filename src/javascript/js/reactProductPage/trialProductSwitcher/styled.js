import styled from "styled-components";

export const TrialWorksTextStyled = styled.div`
    font-size: 14px;
    margin-bottom: 10px;
    text-decoration: none;
    color: #f07826;
    cursor: pointer;
    -webkit-transition: all 0.5s ease-out;
    transition: transform 0.5s ease-out;
    text-align: end;
`;
