import React from "react";
import renderer from "react-test-renderer";
import ReactDom from "react-dom";

import TrialProductSwitcher from ".";

let container = null;

beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    ReactDom.unmountComponentAtNode(container);
    container.remove();
    container = null;
});

jest.mock("./accordionSection", () => ({
    TrialAccordionContent: jest.fn(() => <div className="accordion-content"></div>),
}));

describe("Accordion section", () => {
    it("renders correctly", () => {
        const tree = renderer.create(<TrialProductSwitcher />).toJSON();
        expect(tree).toMatchSnapshot();
    });
});
