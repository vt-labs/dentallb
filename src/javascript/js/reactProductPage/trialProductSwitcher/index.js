import React, { useState } from "react";

import Accordion from "Ui/accordionWrapper";
import { TrialWorksTextStyled } from "./styled";
import SelectOptionButton from "Ui/selectOptionWrapper";
import { TrialAccordionContent } from "./accordionSection";
import { accordionContentOptions } from "./trialAccordionContent";

const TrialProductSwitcher = ({ onClick, isActive, buttonContent }) => {
    const [isOpened, setIsOpened] = useState(true);
    const { buttonText = "Try this for $1", aboveBtnText = "See how trial works" } =
        buttonContent || {};
    const accordionContent = accordionContentOptions(!!buttonContent);
    return (
        <div className="btn-wrapper" onClick={() => isActive && setIsOpened(!isOpened)}>
            <TrialWorksTextStyled isActive={isActive} onClick={onClick}>
                {aboveBtnText}
            </TrialWorksTextStyled>
            <SelectOptionButton isActive={isActive} onClick={onClick}>
                <h4>{buttonText}</h4>
            </SelectOptionButton>
            <Accordion isOpened={isActive && isOpened}>
                <TrialAccordionContent accordionContent={accordionContent} />
            </Accordion>
        </div>
    );
};

export default TrialProductSwitcher;
