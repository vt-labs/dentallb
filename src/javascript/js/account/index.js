import { ReorderClass } from "../reorderClass";
import { getOrderNumber } from "globalServices";

export const accountRepurchase = () => {
    const reorderBtnList = document.querySelectorAll(".js-reorderBtn");
    for (const reorderBtn of reorderBtnList) {
        reorderBtn.addEventListener("click", (e) => {
            new ReorderClass(getOrderNumber(e.target)).addOrderItemsToCart();
        });
    }
};
