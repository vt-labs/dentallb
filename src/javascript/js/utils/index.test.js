import { isArrayEquals, updateProductUrl } from "./";

jest.mock("globalServices", () => ({
    getProductOptions: (variants) => variants,
}));

describe("isArrayEquals", () => {
    const COMPARABLE_ARRAY = ["any", "any1"];
    const TEST_ARRAY_TO_COMPARE = ["any", "any2"];
    const TEST_ARRAY_TO_COMPARE_EQUAL = ["any", "any1"];

    test("equal arrays", () => {
        expect(isArrayEquals(COMPARABLE_ARRAY, TEST_ARRAY_TO_COMPARE_EQUAL)).toBe(true);
    });
    test("unequal arrays", () => {
        expect(isArrayEquals(COMPARABLE_ARRAY, TEST_ARRAY_TO_COMPARE)).toBe(false);
    });
});

describe("UpdateProductUrl test changed url", () => {
    test("UpdateProductUrl should change url", () => {
        const IS_ORIGIN_PRODUCT_TRUE = true;
        const IS_ORIGIN_PRODUCT_FALSE = false;
        const EXPECTED_BUY_NOW_LINK = "/products/buy_now_link";
        const EXPECTED_FREE_TRIAL_LINK = "/products/free_trial_link";

        window.relevantProductLinks = {
            buy_now_link: "buy_now_link",
            free_trial_link: "free_trial_link",
        };

        expect(window.location.pathname).toBe("/");
        updateProductUrl({ isOrigin: IS_ORIGIN_PRODUCT_TRUE });
        expect(window.location.pathname).toBe(EXPECTED_BUY_NOW_LINK);
        updateProductUrl({ isOrigin: IS_ORIGIN_PRODUCT_FALSE });
        expect(window.location.pathname).toBe(EXPECTED_FREE_TRIAL_LINK);
    });
});
