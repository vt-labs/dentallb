import Title from "../reactProductPage/title";
import PaymentTermsInfo from "../reactProductPage/paymentTermsInfo";
import TrialProductSwitcher from "../reactProductPage/trialProductSwitcher";
import ProductOptions from "../reactProductPage/productOptions";
import OriginProductSwitcher from "../reactProductPage/originProductSwitcher";
import Price from "../reactProductPage/price";
import AddToCartBtn from "../reactProductPage/addToCartBtn";
import SubscriptionProductOptions from "../reactProductPage/subscriptionsProductOptions";

export const isIos = () => {
    return (
        ["iPad Simulator", "iPhone Simulator", "iPod Simulator", "iPad", "iPhone", "iPod"].includes(
            navigator.platform
        ) ||
        (navigator.userAgent.includes("Mac") && "ontouchend" in document)
    );
};
const CENTS_IN_DOLLAR = 100;
const DEFAULT_NUMBER_OF_DECIMALS = 2;

export const formatPriceForLayout = (value, decimals) =>
    (+value / CENTS_IN_DOLLAR).toFixed(
        decimals === undefined ? DEFAULT_NUMBER_OF_DECIMALS : decimals
    );

export const updateOriginProductPage = (state) => {
    const stepClick = document.querySelector(".js-buy-now-wrapper");
    const freeTrial = document.querySelector(".js-free-trial-wrapper");
    const stepBarFour = document.querySelector(".js-pdp-step-bar-four");
    const paymentTerms = document.querySelector("#paymentTermsInfoRoot");
    showHideElement(stepClick, state);
    showHideElement(freeTrial, !state);
    showHideElement(paymentTerms, state);
    changeStepNumber(stepBarFour, state);
};

const showHideElement = (element, state) => (element.style.display = state ? "block" : "none");

const changeStepNumber = (element, state) => {
    const STEP_NUMBER_TWO = 2;
    const STEP_NUMBER_THREE = 3;
    element.querySelector(".number").innerHTML = state ? STEP_NUMBER_THREE : STEP_NUMBER_TWO;
};

export const updateProductUrl = ({ isOrigin }) => {
    const {
        history,
        relevantProductLinks,
        location: { pathname },
    } = window;
    if (relevantProductLinks) {
        const { buy_now_link, subscription_link, free_trial_link } = relevantProductLinks;
        const URL = isOrigin
            ? buy_now_link
            : (subscription_link && subscription_link[0]) || free_trial_link;
        !pathname.includes(URL) && history.pushState("", "", `/products/${URL}`);
    }
};

export const rendererLocator = {
    trialButton: {
        rootSelector: "#trialSwitcherRoot",
        Component: TrialProductSwitcher,
    },
    originButton: {
        rootSelector: "#buynowSwitcherRoot",
        Component: OriginProductSwitcher,
    },
    optionsBlock: {
        rootSelector: "#optionsBlockRoot",
        Component: ProductOptions,
    },
    paymentTermsInfo: {
        rootSelector: "#paymentTermsInfoRoot",
        Component: PaymentTermsInfo,
    },
    productTitle: {
        rootSelector: "#productTitleRoot",
        Component: Title,
    },
    productPrice: {
        rootSelector: "#productPriceRoot",
        Component: Price,
    },
    subscriptionButton: {
        rootSelector: "#subscriptionSwitcherRoot",
        Component: TrialProductSwitcher,
    },
    addToCartButton: {
        rootSelector: "#addToCartRoot",
        Component: AddToCartBtn,
    },
    subscriptionOptions: {
        rootSelector: "#subscriptionOptionsRoot",
        Component: SubscriptionProductOptions,
    },
};

export const getVariantId = (variants) => {
    const selectors = document.querySelectorAll(".product-options");
    const selectedOptionsArray = [];
    for (const selector of selectors) {
        selectedOptionsArray.push(selector.querySelector("select").value);
    }
    const selectedVariant = variants.find(({ options }) =>
        isArrayEquals(options, selectedOptionsArray)
    );
    sessionStorage.setItem("selectedProductVariantId", selectedVariant?.id);
};

export const manageNextButtons = (isOptionsValidated, setPopupState) => {
    const formsList = document.querySelector(".js-step-click");
    const trtBnt = document.querySelector(".js-free-trial");

    const nextFakeBtn = document.querySelectorAll(".first-fake");
    setPopupState(false);
    if (isOptionsValidated) {
        showHideElement(formsList, true);
        showHideElement(trtBnt, true);
        nextFakeBtn.forEach((nextFakeBtn) => {
            showHideElement(nextFakeBtn, false);
        });
    } else {
        nextFakeBtn.forEach((nextFakeBtn) => {
            showHideElement(nextFakeBtn, true);
        });
        showHideElement(formsList, false);
        showHideElement(trtBnt, false);
    }
    nextFakeBtn.forEach((nextFakeBtn) => {
        nextFakeBtn.addEventListener("click", () => {
            !isOptionsValidated && setPopupState(true);
        });
    });
};

export const optionsValidation = (selectedOptions, options) =>
    selectedOptions.length === options.length && !selectedOptions.includes("None selected");

export const isArrayEquals = (a, b) => {
    if (a.length !== b.length) {
        return false;
    }
    const uniqueValues = new Set([...a, ...b]);
    for (const v of uniqueValues) {
        const aCount = a.filter((e) => e === v).length;
        const bCount = b.filter((e) => e === v).length;
        if (aCount !== bCount) {
            return false;
        }
    }
    return true;
};

export const getProductState = () => {
    const {
        location: { pathname },
        relevantProductLinks: { buy_now_link },
    } = window;
    return pathname.includes(buy_now_link);
};

export const klaviyoAddToCartEvent = (cartData) => {
    // eslint-disable-next-line no-use-before-define
    const _learnq = _learnq || [];
    const cartInfo = {
        total_price: +cartData.totalPrice / CENTS_IN_DOLLAR,
        $value: +cartData.totalPrice / CENTS_IN_DOLLAR,
        total_discount: +cartData.totalDiscount,
        original_total_price: +cartData.originalTotalPrice / CENTS_IN_DOLLAR,
        items: cartData.lineItems,
    };
    _learnq.push(["track", "Added to Cart", cartInfo]);
};
