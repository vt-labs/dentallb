export const isDefaultOptions = (options) => options.includes("Title");

export const getProductOptions = (productData) => {
    const { options, variants } = productData;
    if (isDefaultOptions(options)) {
        return [];
    }
    const optionArraysObject = options.map((item, index) => ({
        optionName: item,
        optionKey: `option${index + 1}`,
    }));
    optionArraysObject.forEach((item) => {
        const allOptionsList = [];
        variants.forEach((variant) => allOptionsList.push(variant[item.optionKey]));
        const newOptionVariants = allOptionsList.filter(
            (element, index, array) => array.indexOf(element) === index
        );
        item.optionVariants = newOptionVariants;
    });
    return optionArraysObject;
};

export const setStoreQuantity = (quantity) => sessionStorage.setItem("quantity", quantity);

export const getStoreQuantity = () => sessionStorage.getItem("quantity");

export const getChosenVariantId = (productVariantsList) => {
    const isAnyDataInStorage =
        sessionStorage.getItem("selectedProductVariantId") !== "undefined" &&
        sessionStorage.getItem("selectedProductVariantId") !== null;
    const chosenVariant = isAnyDataInStorage
        ? sessionStorage.getItem("selectedProductVariantId")
        : productVariantsList[0].id;
    sessionStorage.setItem("selectedProductVariantId", chosenVariant);
    return chosenVariant;
};

export const clearChosenVariantId = () => sessionStorage.removeItem("selectedProductVariantId");

export const getOrderNumber = (clickedBtn) => clickedBtn.dataset.orderNumber;

export const getSellingPlanID = (productObject) => {
    const { selling_plan_groups } = productObject;
    if (!selling_plan_groups?.length) {
        return null;
    }
    const [
        {
            selling_plans: [{ id }],
        },
    ] = selling_plan_groups;
    return id;
};
