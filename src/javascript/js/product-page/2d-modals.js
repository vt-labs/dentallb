import {
    getsecondStepEle,
    openModal,
    addValidated,
    removeValidated,
    modalManagement,
    getArrayFromNodes,
    validateSelectsForModal,
    setValidated,
    showHideNextBtn,
    buildStateArray,
} from "./utils";
export const twoDProductModal = () => {
    const secondStep = getsecondStepEle();
    const form = secondStep.querySelector(".two-d-form");
    if (!form) {
        return null;
    }
    const nextStepBtn = secondStep.querySelector(".nextStepBtn");
    const nextBtn = nextStepBtn.querySelector("a");
    const fakeNextBtn = nextStepBtn.querySelector("span");
    const selectWrappersList = secondStep.querySelectorAll(".selector-wrapper-custom");
    const validImpressionModal = document.querySelector("#validImpressionModal");
    const invalidImpressionModal = document.querySelector("#invalidImpressionModal");
    const titleOfInvalidModal = invalidImpressionModal.querySelector(".title");
    const TITLE_CONTENT = `No worries! We’ll send you a new kit if we don’t have your mold stored at our lab`;
    const selectArray = getArrayFromNodes(selectWrappersList);
    const stateArray = buildStateArray(selectArray);
    const pasteModalTitleContent = (content) => {
        titleOfInvalidModal.innerHTML = content;
    };

    const validateSelects = () => {
        selectArray.forEach((select, index) => {
            const selectItem = select.querySelector("select");
            const validationData = {
                selectItem,
                index,
                stateArray,
            };
            if (!index) {
                if (selectItem.value === "") {
                    addValidated(select);
                } else {
                    removeValidated(select);
                    setValidated(validationData);
                }
            } else if (stateArray[0].validated && stateArray[0].value === "yes") {
                selectItem.removeAttribute("disabled");
                if (selectItem.value === "") {
                    addValidated(select);
                    stateArray[index].validated = false;
                } else {
                    removeValidated(select);
                    setValidated(validationData);
                }
            } else {
                removeValidated(select);
                setValidated(validationData);
                selectItem.setAttribute("disabled", "disabled");
            }
        });
        return !stateArray.filter(({ validated }) => !validated).length;
    };

    const selectorsValidation = () => {
        fakeNextBtn.addEventListener("click", function () {
            validateSelects();
        });

        form.addEventListener("change", () => {
            const nextBtnsObject = {
                fakeNextBtn,
                nextBtn,
            };
            showHideNextBtn(nextBtnsObject, validateSelects());
        });
        nextBtn.addEventListener("click", () => {
            if (validateSelectsForModal(stateArray)) {
                openModal(validImpressionModal);
                localStorage.setItem("impression", "valid");
            } else {
                stateArray[0].value === "no" && pasteModalTitleContent(TITLE_CONTENT);
                openModal(invalidImpressionModal);
                localStorage.setItem("impression", "needed");
            }
        });
    };

    selectorsValidation();
    modalManagement(validImpressionModal);
    modalManagement(invalidImpressionModal);
};
