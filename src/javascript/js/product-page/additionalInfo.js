export const additionalInfoSection = () => {
    const additionalSectionsTitleBlocks = document.querySelectorAll(".info-section__title");
    const contentElementsList = document.querySelectorAll(".info-section__content");
    for (const additionalSectionsTitleBlock of additionalSectionsTitleBlocks) {
        additionalSectionsTitleBlock.addEventListener("click", (e) => {
            additionalSectionsTitleBlocks.forEach((item) => item.classList.remove("active"));
            contentElementsList.forEach((item) => item.classList.remove("active"));
            e.target.classList.add("active");
            const sectionTypeAttribute = e.target.getAttribute("data-section-type");
            contentElementsList.forEach(
                (item) =>
                    item.getAttribute("data-section-type") === sectionTypeAttribute &&
                    item.classList.add("active")
            );
        });
    }
};
