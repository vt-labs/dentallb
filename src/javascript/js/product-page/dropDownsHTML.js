export const threeDOptionsHTML = `
      <div class="selector-wrapper-custom js product-form__item ">
        <label for="SingleOptionSelector-{{ section.id }}">
          Do you have an existing UPPER impression that is 
          under 24 months old and your UPPER teeth have not 
          had any recent dental work done?
        </label>          
        <select class="single-option-selector single-option-selector-{{ section.id }}
                      product-form__input"  data-index="option">           
            <option value="" disabled selected hidden>Please Choose...</option>
            <option value="yes" >Yes</option>
            <option value="no"> No </option>            
        </select>
        <div class="validation-message">Please choose one</div>
      </div>
      <div class="selector-wrapper-custom js product-form__item">
        <label for="SingleOptionSelector-{{ section.id }}">
          Do you have an existing LOWER impression that is under
          24 months old your LOWER teeth have not had any recent
          dental work done?
        </label>
        <select class="single-option-selector single-option-selector-{{ section.id }}
                      product-form__input"  data-index="option">           
          <option value="" disabled selected hidden >Please Choose...</option>  
          <option value="yes" >Yes</option>
          <option value="no" > No</option>            
        </select>
        <div class="validation-message">Please choose one</div>
      </div>`;
export const twoDOptionsHTML = `
      <div class="selector-wrapper-custom js product-form__item ">
          <label for="SingleOptionSelector-{{ section.id }}">
          Still have your previous impression?
          </label>          
          <select class="single-option-selector single-option-selector-{{ section.id }}
                          product-form__input"  data-index="option">           
            <option value="" disabled selected hidden>Please Choose...</option>
            <option value="yes" >Yes</option>
            <option value="no"> No </option>            
          </select>
          <div class="validation-message">Please choose one</div>
        </div>
        <div class="selector-wrapper-custom js product-form__item">
            <label for="SingleOptionSelector-{{ section.id }}">
            Have your teeth changed due to dental work or a missing tooth?
            </label>
              <select class="single-option-selector 
              single-option-selector-{{ section.id }} product-form__input"  data-index="option">           
                <option value="" disabled selected hidden >Please Choose...</option>  
                <option value="no" >Yes</option>
                <option value="yes" >No</option>>            
              </select>
              <div class="validation-message">Please choose one</div>
          </div>`;
