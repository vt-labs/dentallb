export const threeDModalHTML = `
<div id="validImpressionModal" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <div class="main-modal-content">
      <div class="title">Great! We can reuse your existing impression + teeth mold.</div>
      <div class="sub-title">
        Upon order completion, please follow the instructions in your email confirmation to return the impression + mold to our lab. If we have your mold stored in our lab, then we’ll process your order shortly.
      </div>
      <span class="btn-red submitBtn"> Continue</span>
    </div>
  </div>
</div>

<div id="mixedImpressionModal" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <div class="main-modal-content">
      <div class="title">mixed impression</div>
      <div class="sub-title">
      </div>
      <span class="btn-red submitBtn"> Continue</span>
    </div>
  </div>
</div>
<div id="invalidImpressionModal" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <div class="main-modal-content">
      <div class="title">Great! We’ll send you a new 3D kit.</div>
      <div class="sub-title">
      </div>
      <span class="btn-red submitBtn"> Continue</span>
    </div>
  </div>
</div>`;

export const twoDModalHTML = `
<div id="validImpressionModal" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <div class="main-modal-content">
      <div class="title">Great! We can reuse your existing impression + teeth mold.</div>
      <div class="sub-title">
      Upon order completion, please follow the instructions in your email confirmation to return the impression + mold to our lab. If we have your mold stored in our lab, then we’ll process your order shortly.
      </div>
      <span class="btn-red submitBtn">Continue</span>
    </div>
  </div>
</div>
<div id="invalidImpressionModal" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <div class="main-modal-content">
      <div class="title">No worries! We’ll send you a new kit</div>
      <div class="sub-title">
      </div>
      <span class="btn-red submitBtn">Continue</span>
    </div>
  </div>
</div>`;
