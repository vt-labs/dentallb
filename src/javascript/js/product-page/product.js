import { getProductIdForShopifyTemplates, is3DProduct } from "./utils";
import { threeDOptionsHTML, twoDOptionsHTML } from "./dropDownsHTML";
import { threeDModalHTML, twoDModalHTML } from "./modalsHTML";
import { threeDProductModal } from "./3d-modals";
import { twoDProductModal } from "./2d-modals";

export const currentVariant = () => {
    const formsList = document.querySelector(".js-step-click");
    const treeDSelectsWrapper = document.querySelector(".three-d-form");
    const twoDSelectsWrapper = document.querySelector(".two-d-form");
    const modalsWrapper = document.querySelector("#modals");

    formsList &&
        formsList.addEventListener("click", () => {
            // eslint-disable-next-line no-undef
            const chosenVariantObject = prodVariants.find(
                ({ id }) => id === +getProductIdForShopifyTemplates()
            );
            if (is3DProduct(chosenVariantObject)) {
                treeDSelectsWrapper.innerHTML = threeDOptionsHTML;
                twoDSelectsWrapper.innerHTML = "";
                modalsWrapper.innerHTML = threeDModalHTML;
                threeDProductModal();
            } else {
                modalsWrapper.innerHTML = twoDModalHTML;
                treeDSelectsWrapper.innerHTML = "";
                twoDSelectsWrapper.innerHTML = twoDOptionsHTML;
                twoDProductModal();
            }
        });
};
