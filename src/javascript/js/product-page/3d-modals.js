import {
    openModal,
    addValidated,
    removeValidated,
    modalManagement,
    getArrayFromNodes,
    validateSelectsForModal,
    setValidated,
    showHideNextBtn,
    buildStateArray,
    getMixedVariant,
} from "./utils";

export const threeDProductModal = () => {
    const secondStep = document.querySelector(".pdp-step-two");
    const form = secondStep.querySelector(".three-d-form");
    if (!form.childNodes.length) {
        return null;
    }
    const nextStepBtn = secondStep.querySelector(".nextStepBtn");
    const nextBtn = nextStepBtn.querySelector("a");
    const fakeNextBtn = nextStepBtn.querySelector("span");
    const selectWrappersList = secondStep.querySelectorAll(".selector-wrapper-custom");
    const validImpressionModal = document.querySelector("#validImpressionModal");
    const invalidImpressionModal = document.querySelector("#invalidImpressionModal");
    const mixedImpressionModal = document.querySelector("#mixedImpressionModal");
    const subtitleOfMixedModal = mixedImpressionModal.querySelector(".sub-title");
    const titleOfMixedModal = mixedImpressionModal.querySelector(".title");
    const MIXED_MODAL_CONTENT = [
        {
            title: "Great! You’ll receive a lower kit",
            body: `Upon order completion, we’ll only send out lower 
                impression materials since you already have an upper 
                impression. Please send back both impressions to our
                 lab after you’ve taken the lower impression.`,
        },
        {
            title: "Great! You’ll receive a upper kit",
            body: `Upon order completion, we’ll only send out upper 
                impression materials since you already have a lower 
                impression. Please send back both impressions to 
                our lab after you’ve taken the upper impression.`,
        },
    ];

    const selectArray = getArrayFromNodes(selectWrappersList);
    const stateArray = buildStateArray(selectArray);

    const pasteModalSubtitleContent = (content) => {
        subtitleOfMixedModal.innerHTML = content.body;
        titleOfMixedModal.innerHTML = content.title;
    };

    const validateSelects = () => {
        selectArray.forEach((select, index) => {
            const selectItem = select.querySelector("select");
            const validationData = {
                selectItem,
                index,
                stateArray,
            };
            if (selectItem.value === "") {
                addValidated(select);
            } else {
                removeValidated(select);
                setValidated(validationData);
            }
        });
        return !stateArray.filter(({ validated }) => !validated).length;
    };

    const selectorsValidation = () => {
        fakeNextBtn.addEventListener("click", function () {
            validateSelects();
        });

        form.addEventListener("change", () => {
            const nextBtnsObject = {
                fakeNextBtn,
                nextBtn,
            };
            showHideNextBtn(nextBtnsObject, validateSelects());
        });
        nextBtn.addEventListener("click", () => {
            if (validateSelectsForModal(stateArray)) {
                openModal(validImpressionModal);
                localStorage.setItem("impression", "valid");
            } else {
                if (getMixedVariant(stateArray) < +0) {
                    openModal(invalidImpressionModal);
                    localStorage.setItem("impression", "needed");
                } else {
                    pasteModalSubtitleContent(MIXED_MODAL_CONTENT[getMixedVariant(stateArray)]);
                    openModal(mixedImpressionModal);
                    localStorage.setItem(
                        "impression",
                        getMixedVariant(stateArray) ? "upper needed" : "lower needed"
                    );
                }
            }
        });
    };

    selectorsValidation();
    modalManagement(validImpressionModal);
    modalManagement(invalidImpressionModal);
    modalManagement(mixedImpressionModal);
};
