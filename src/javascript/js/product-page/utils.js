export const getsecondStepEle = () => document.querySelector(".pdp-step-two");
export const openModal = (element) => {
    element.style.display = "block";
};
export const addValidated = (element) => {
    element.classList.add("notValidated");
};
export const removeValidated = (element) => {
    element.classList.remove("notValidated");
};

export const modalManagement = (modalElement) => {
    const crossBtn = modalElement.querySelector(".close");
    const submitBtn = modalElement.querySelector(".submitBtn");
    const modalID = modalElement.id;

    crossBtn.addEventListener("click", () => {
        closeModal(modalElement);
    });
    submitBtn.addEventListener("click", () => {
        closeModal(modalElement);
    });
    modalElement.addEventListener("click", (e) => {
        e.stopPropagation();
        e.target.id === modalID && closeModal(modalElement);
    });
};

export const getArrayFromNodes = (nodes) =>
    Array.from({ length: nodes.length }, (_, i) => nodes[i]);

export const getProductIdForShopifyTemplates = () => {
    const urlParams = new URLSearchParams(window.location.search);
    const variantIdFromUrl = urlParams.get("variant");
    const variantIdFromDataAttribute = document.querySelector(".product-id").dataset.productid;
    return variantIdFromUrl ? +variantIdFromUrl : +variantIdFromDataAttribute;
};

export const is3DProduct = (variantObject) =>
    variantObject.title.includes("3D") || variantObject.sku.includes("3D");

export const validateSelectsForModal = (stateArray) => {
    const notValidatedArray = stateArray.filter(({ validated }) => !validated);
    const valueValidationArray = stateArray.filter(({ value }) => value === "no");
    return !notValidatedArray.length && !valueValidationArray.length;
};

export const setValidated = (validationData) => {
    const { selectItem, index, stateArray } = validationData;
    stateArray[index].validated = true;
    stateArray[index].value = selectItem.value;
};

export const showHideNextBtn = (nextBtnsObject, isSelectsValidated) => {
    const { fakeNextBtn, nextBtn } = nextBtnsObject;
    if (isSelectsValidated) {
        fakeNextBtn.classList.add("hide");
        nextBtn.classList.remove("hide");
    } else {
        fakeNextBtn.classList.remove("hide");
        nextBtn.classList.add("hide");
    }
};

export const buildStateArray = (selectArray) =>
    selectArray.map(() => {
        return { validated: false, value: "" };
    });

export const getMixedVariant = (stateArray) => stateArray.findIndex((item) => item.value === "yes");

const closeModal = (element) => {
    element.style.display = "none";
};

export const getProductId = () => {
    const sessionStorageIdData = sessionStorage.getItem("selectedProductVariantId");
    return sessionStorageIdData ? sessionStorageIdData : getProductIdForShopifyTemplates();
};
