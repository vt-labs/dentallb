export const preLoader = () => {
    const cartDrawer = document.querySelector(".cart-drawer");
    const loader = document.createElement("div");
    const addLoader = () => {
        loader.className = "cart-drawer-loader";
        cartDrawer.appendChild(loader);
    };

    addLoader();
    const displayLoading = () => {
        cartDrawer.classList.add("loading");
    };

    const cartDrawerFooterCheckoutBtn = document.querySelector(".cart-drawer-checkout-btn");

    cartDrawerFooterCheckoutBtn.addEventListener("click", () => {
        displayLoading();
    });
};
