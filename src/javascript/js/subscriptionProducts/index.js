import { getChosenVariantId } from "globalServices";

class SubscriptionProductsClass {
    constructor() {
        this.subscriptionOptionsList = window.subscriptionProductOptions;
        this.getSelectedSubscriptionProduct = this.getSelectedSubscriptionProduct.bind(this);
        this.subscriptionProductsList = window.subscriptionProducts;
    }

    getSubscriptionOptionList() {
        return this.subscriptionOptionsList;
    }

    getSelectedSubscriptionProduct(option) {
        const optionIndex = this.subscriptionOptionsList.findIndex(
            (optionItem) => optionItem === option
        );
        const product = this.subscriptionProductsList[optionIndex];
        const activeVariant = getChosenVariantId(product.variants);
        return { product, activeVariant };
    }
}
export default SubscriptionProductsClass;
