import { isIos, formatPriceForLayout } from "globalUtils";
import { Cart, CartDataService } from "../cart";
import { getProductId } from "../product-page/utils";
import { getStoreQuantity } from "globalServices";
import { emptyCartHTML, lineItemHML } from "../cart/cartHtml";

export const cart = new Cart(new CartDataService());
const nodeBody = document.querySelector("body");
const cartDrawer = document.querySelector(".cart-drawer");
const cartDrawerBody = document.querySelector(".cart-drawer-body");
const headerCartBtn = document.querySelector(".header .link-cart");
const cartDrawerCloseBtn = document.querySelector(".close-cart-drawer");
const cartDrawerOverlay = document.querySelector(".cart-drawer-overlay");
const cartItemsCounterBadge = document.querySelector(".link-cart .icon-count");
const cartDrawerItemsCounter = document.querySelector(".cart-drawer-header .cart-items-count");
const cartDrawerFooter = document.querySelector(".cart-drawer-footer");
const cartDrawerFooterCheckoutTotalTitle = cartDrawerFooter.querySelector(".checkout-total .title");
const cartDrawerFooterCheckoutTotalCost = cartDrawerFooter.querySelector(".checkout-total .cost");
const cartDrawerFooterCheckoutYouSave = cartDrawerFooter.querySelector(".you-save-line");

const isCartDrawerOpen = () => nodeBody.classList.contains("show-cart-drawer");
// eslint-disable-next-line no-use-before-define
const _learnq = _learnq || [];

export const drawCartData = async () => {
    const data = await cart.getValues();
    CartDrawer.updateCartItemsCounterBadge(data.itemsCount);
    CartDrawer.renderCartDrawerLineItems(data);
    !isCartDrawerOpen() && CartDrawer.showCartDrawer();
};

const handleAddProductToCart = (e) => {
    const productId = getProductId();
    const sellingPlan = e.target.dataset.sellingplanid;
    const quantity = getStoreQuantity();
    const data = { items: [{ id: productId, selling_plan: sellingPlan, quantity }] };
    cart.addItem(data).then(() => drawCartData());
};

const CART_EMPTY_CLASS = "cart-empty";
const LINE_ITEM_DATA_HOLDER_CLASS = ".line-item-data-holder";

const CartDrawer = {
    init() {
        this.initCartDrawerBtns();
        this.initRenderCartDrawerData();
    },

    initCartDrawerBtns() {
        headerCartBtn.addEventListener("click", this.showCartDrawer);
        cartDrawerCloseBtn.addEventListener("click", this.hideCartDrawer);
        cartDrawerOverlay.addEventListener("click", this.hideCartDrawer);
    },

    async initRenderCartDrawerData() {
        const data = await cart.getValues();
        if (data.lineItems.length > 0) {
            this.updateCartItemsCounterBadge(data.itemsCount);
            cartDrawer.classList.remove(CART_EMPTY_CLASS);
            this.renderCartDrawerLineItems(data);
        } else {
            cartDrawer.classList.add(CART_EMPTY_CLASS);
        }
        this.initEventListeners();
    },

    updateCartItemsCounterBadge(value) {
        cartItemsCounterBadge.innerText = value ? value : "0";
        cartDrawerItemsCounter.innerHTML = value > 0 ? `(${value})` : "";
    },

    renderCartDrawerLineItems(data) {
        const { lineItems, totalPrice, totalDiscount } = data;
        if (lineItems.length === 0) {
            cartDrawer.classList.add(CART_EMPTY_CLASS);
            cartDrawerBody.innerHTML = emptyCartHTML;
            CartDrawer.showCartDrawerTotal();
            CartDrawer.calcCartDrawerBodyHeight();
        } else {
            cartDrawer.classList.remove(CART_EMPTY_CLASS);
            const cartItems = lineItems.map((item) => lineItemHML(item));
            cartDrawerBody.innerHTML = `<ul class="list-unstyled product-list-min">
                                            ${cartItems.join("")}
                                        </ul>`;
            CartDrawer.showCartDrawerTotal(totalPrice, totalDiscount);
            CartDrawer.calcCartDrawerBodyHeight();
        }
        CartDrawer.initEventListeners();
    },

    initEventListeners() {
        const changeQuantityBtns = document.querySelectorAll(".update-quantity");
        const changeQuantityInput = document.querySelectorAll(".update-quantity-input");
        const cartDrawerAddBtns = document.querySelectorAll(
            "#freeTrialAddToCart, #productTryNowBtn"
        );
        changeQuantityBtns.forEach((btn) =>
            btn.addEventListener("click", CartDrawer.handleQuantityBtnsClick)
        );
        changeQuantityInput.forEach((input) =>
            input.addEventListener("change", CartDrawer.handleQuantityInputChange)
        );
        cartDrawerAddBtns.forEach((btn) => btn.addEventListener("click", handleAddProductToCart));
    },

    handleQuantityBtnsClick(e) {
        e.preventDefault();
        const element = e.target;
        const action = element.closest(".update-quantity").dataset.quantity;
        const variantId = element.closest(LINE_ITEM_DATA_HOLDER_CLASS).dataset.cartItemVariantId;
        const variantQty = element.closest(LINE_ITEM_DATA_HOLDER_CLASS).dataset.cartItemQuantity;
        const quantityActions = {
            plus: +variantQty + 1,
            minus: +variantQty - 1,
        };
        const qty = quantityActions[action] || 0;
        cart.updateItem(variantId, qty).then(() => drawCartData());
    },

    handleQuantityInputChange(e) {
        const element = e.target;
        const variantId = element.closest(LINE_ITEM_DATA_HOLDER_CLASS).dataset.cartItemVariantId;
        const quantity = element.value;
        quantity >= 0 && cart.updateItem(variantId, quantity).then(() => drawCartData());
    },

    showCartDrawerTotal(totalPrice, totalDiscount) {
        cartDrawerFooterCheckoutTotalTitle.innerText = totalPrice ? "Your Subtotal" : "";
        cartDrawerFooterCheckoutTotalCost.innerText = totalPrice
            ? formatPriceForLayout(totalPrice)
            : null;
        cartDrawerFooterCheckoutYouSave.innerHTML = totalDiscount
            ? `You save ${formatPriceForLayout(totalDiscount)}!`
            : "";
    },

    calcCartDrawerBodyHeight() {
        const cardDrawerHeader = document.querySelector(".cart-drawer-header").offsetHeight;
        const cardDrawerFooter = document.querySelector(".cart-drawer-footer").offsetHeight;
        const cardDrawerPromoBanner = document.querySelector(".cart-drawer .promocode");
        const maxHeightDelta = +cardDrawerPromoBanner?.offsetHeight;
        const differenceHeight = cardDrawerHeader + cardDrawerFooter + maxHeightDelta;
        const additionalGapOnScrolling = isIos() && window.scrollY > 1 ? +"100" : 0;
        const cardDrawerHeight = document.querySelector(".cart-drawer").offsetHeight;
        const maxHeight = cardDrawerHeight - additionalGapOnScrolling - differenceHeight;
        cartDrawerBody.style.maxHeight = `${maxHeight}px`;
    },

    showCartDrawer() {
        CartDrawer.calcCartDrawerBodyHeight();
        nodeBody.classList.add("show-cart-drawer");
    },

    hideCartDrawer() {
        nodeBody.classList.remove("show-cart-drawer");
    },
};

CartDrawer.init();
