import { OrderDAO } from "./";

window.ordersListData = [
    { orderNumber: "12", itemsList: [{ variant: { id: "11" }, quantity: 1 }] },
    { orderNumber: "123", itemsList: [{ variant: { id: "22" }, quantity: 1 }] },
];
describe("OrderDAO class", () => {
    test("OrderDAO class get correct order data", () => {
        const orderItemsList = new OrderDAO("12").getOrderItems();
        const [{ quantity, id }] = orderItemsList;
        expect(orderItemsList.length).toBe(1);
        expect(quantity).toBe(1);
        expect(id).toBe("11");
    });
});
