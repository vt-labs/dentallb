export class OrderDAO {
    constructor(orderNumber) {
        this.orderNumber = orderNumber;
    }

    getOrderItems() {
        const { ordersListData } = window;
        const orderData = ordersListData.find(
            ({ orderNumber }) => orderNumber === this.orderNumber
        );
        const { itemsList } = orderData;
        return itemsList.map(({ quantity, variant: { id } }) => {
            return { quantity, id };
        });
    }
}
