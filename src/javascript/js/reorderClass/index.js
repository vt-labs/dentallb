import { OrderDAO } from "../orderDAO";
import { Cart, CartDataService } from "../cart";
import { drawCartData } from "../cartInit";

export class ReorderClass {
    constructor(orderNumber) {
        this.cart = new Cart(new CartDataService());
        this.orderItemsList = new OrderDAO(orderNumber).getOrderItems();
    }

    addOrderItemsToCart() {
        this.cart.addItem({ items: this.orderItemsList }).then(() => {
            drawCartData();
        });
    }
}
