class CustomerDAO {
    constructor() {
        this.isLogged = window.isLogged;
        this.isAnyCustomerStatus = window.isAnyCustomerStatus;
    }

    getCustomer() {
        return this.isAnyCustomerStatus
            ? { isAnyCustomerStatus: this.isAnyCustomerStatus }
            : { isLogged: this.isLogged };
    }
}
export default CustomerDAO;
