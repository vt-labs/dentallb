import { Cart, CartDataService } from "../cart";
import { getProductId } from "../product-page/utils";
import { getStoreQuantity, setStoreQuantity } from "globalServices";
import { drawCartData } from "../cartInit";

export const productDetails = () => {
    const cart = new Cart(new CartDataService());
    // eslint-disable-next-line no-use-before-define
    const _learnq = _learnq || [];
    $(document).ready(function ($) {
        sessionStorage.removeItem("selectedProductVariantId");
        $(".js-pdp-image-block li:first-child").addClass("active");
        $(".js-pdp-thumbnailimage-block li:first-child").addClass("active");
        $(".js-pdp-thumbnailimage-block li").click(function (e) {
            $(".js-pdp-image-block li").removeClass("active");
            $(".js-pdp-thumbnailimage-block li").removeClass("active");
            const childIndex = Number($(this).index()) + 1;
            $(".js-pdp-image-block li:nth-child(" + childIndex + ")").addClass("active");
            $(".js-pdp-thumbnailimage-block li:nth-child(" + childIndex + ")").addClass("active");
        });
        setStoreQuantity(1);
        $(".js-product-single__quantity .js-minus-qty").click(function () {
            let productQuantity = $(".js-quantity-selector").val();
            if (productQuantity > 1) {
                productQuantity--;
            }
            $(".js-quantity-selector").val(productQuantity);
            setStoreQuantity(productQuantity);
        });
        $(".js-product-single__quantity .js-plus-qty").click(function () {
            let productQuantity = $(".js-quantity-selector").val();
            productQuantity++;
            $(".js-quantity-selector").val(productQuantity);
            setStoreQuantity(productQuantity);
        });
        /* Pop up open for the shopping experience
Insurance, addons
*/
        $(".js-popup-open").click(function () {
            window.scrollTo(0, 0);
            $("#pdp-popup").show();
        });
        /* Pop up close for the shopping experience */
        $(".js-popup-close").click(function () {
            $("#pdp-popup").hide();
        });
        /* Checkout button */
        $(".js-step-click").click(function () {
            /* check if multiple people option is checked
then show the notice to edit the quantity on the cart page */
            let multiplePeopleCheckboxChecked = false;
            if ($("#multiplePeople:checked").length > 0) {
                $(".js-multiple-people").removeClass("hide");
                multiplePeopleCheckboxChecked = true;
            } else {
                $(".js-multiple-people").addClass("hide");
            }
            const step = $(this).attr("data-attr-step");
            /* If 4th Step - Add to cart */
            if (step === "four") {
                if (!sessionStorage.getItem("products")) {
                    if ($(this).hasClass("disabled")) {
                        return;
                    } else {
                        /* For the main product */
                        const prodId = getProductId();
                        const quantity = getStoreQuantity();
                        const ids = [];
                        ids.push({ [prodId]: quantity });
                        sessionStorage.setItem("products", JSON.stringify(ids));
                    }
                }
                const addOnIds = JSON.parse(sessionStorage.getItem("addOnProducts")) ?? [];
                const main = JSON.parse(sessionStorage.getItem("products")) ?? [];
                let ProductSellingPlanID = $(".buy-now-button")[0].dataset.sellingplanid;
                // eslint-disable-next-line no-undef
                if (!$.inArray(ProductSellingPlanID, subscriptionLib)) {
                    console.log("success");
                    ProductSellingPlanID = "";
                }
                /* insurance recharge */
                const insurance = JSON.parse(sessionStorage.getItem("insurance")) ?? "";
                let InsuranceSellingPlanID = sessionStorage.getItem("InsuranceSellingPlanID");
                if (InsuranceSellingPlanID !== "undefined") {
                    InsuranceSellingPlanID = JSON.parse(InsuranceSellingPlanID) ?? "";
                    InsuranceSellingPlanID = Number(InsuranceSellingPlanID);
                } else {
                    InsuranceSellingPlanID = "";
                }
                const items = [];
                // Add main product.
                const key = Object.keys(main[0])[0];
                /* Check if the customer came frm the addone collection url with the token
If yes, then add a line item Add-On: Yes
*/
                const lineProperties = {};
                lineProperties["_main-product"] = "Yes";
                // eslint-disable-next-line no-undef
                const addonCookieValue = getCookie("addonCookie");
                if (addonCookieValue !== undefined) {
                    lineProperties["Add-On"] = "Yes";
                }
                /* Check if the customer has previously placed any orders
If yes, then add a line item Return Customer: Yes
*/
                if (localStorage.checkReturningCustomer) {
                    lineProperties["Return Customer"] = "Yes";
                }
                /* Check if multiple people option is checked
If yes, then add a line item Multiple People: Yes
*/
                if (multiplePeopleCheckboxChecked) {
                    lineProperties["Multiple People"] = "Yes";
                }
                if (localStorage.multiplePeopleAnswer) {
                    lineProperties["Multiple People"] = localStorage.multiplePeopleAnswer;
                }
                if (localStorage.quizitem) {
                    if (key === localStorage.quizitem) {
                        lineProperties._Quiz = "Yes";
                    }
                }
                /* For the main item */
                const mainItemQuantity = Number(main[0][key]);
                if (ProductSellingPlanID !== "") {
                    items.push({
                        id: Number(Object.keys(main[0])[0]),
                        quantity: mainItemQuantity,
                        selling_plan: ProductSellingPlanID,
                        properties: lineProperties,
                    });
                } else {
                    items.push({
                        id: Number(Object.keys(main[0])[0]),
                        quantity: mainItemQuantity,
                        properties: lineProperties,
                    });
                }
                // Add insurance recharge product.
                if (insurance) {
                    if (InsuranceSellingPlanID !== "") {
                        items.push({
                            id: Number(insurance),
                            quantity: 1,
                            selling_plan: InsuranceSellingPlanID,
                        });
                    } else {
                        items.push({
                            id: Number(insurance),
                            quantity: 1,
                        });
                    }
                }
                // Add Add-on products.
                addOnIds.forEach((elem) => {
                    items.push({
                        id: elem,
                        quantity: 1,
                    });
                });
                const impressionDataFromStoradge = localStorage.getItem("impression");
                impressionDataFromStoradge &&
                    (lineProperties.Impression = impressionDataFromStoradge);
                _learnq.push(["track", "Added to Cart", items]);
                if (getExistingItems(items).length) {
                    const itemInCart = getExistingItems(items)[0];
                    const line = getItemCartIndex(itemInCart);
                    const properties = itemInCart.properties;
                    cart.changeItem(line, properties).then((response) => {
                        localStorage.removeItem("impression");
                        sessionStorage.removeItem("products");
                        if (getNewItems(items).length) {
                            const newItemsList = getNewItems(items);
                            const formData = {
                                items: newItemsList,
                            };
                            cart.addItem(formData).then(() => {
                                sessionStorage.removeItem("addOnProducts");
                                sessionStorage.removeItem("insurance");
                                drawCartData();
                            });
                        } else {
                            drawCartData();
                        }
                    });
                } else if (getNewItems(items).length) {
                    const newItemsList = getNewItems(items);
                    const formData = {
                        items: newItemsList,
                    };
                    cart.addItem(formData).then(() => {
                        sessionStorage.removeItem("products");
                        sessionStorage.removeItem("addOnProducts");
                        sessionStorage.removeItem("insurance");
                        localStorage.removeItem("impression");
                        drawCartData();
                    });
                }
            } else {
                $(".pdp-steps").hide();
                $(".pdp-steps").removeClass("active");
                $(".pdp-step-" + step).show();
                $(".pdp-step-" + step).addClass("active");
                $(".pdp-step-bar li").removeClass("active");
                $(".js-pdp-step-bar-" + step).addClass("active");
                if (step === "three") {
                    // Insurance product.
                    if ($("#add-insurance").hasClass("tick-box")) {
                        const prodId = $("#add-insurance").data("pid");
                        const InsuranceSellingPlanID = $("#add-insurance")
                            .attr("data-sellingplanidInsurance")
                            .toString();
                        sessionStorage.setItem("insurance", JSON.stringify(prodId));
                        sessionStorage.setItem(
                            "InsuranceSellingPlanID",
                            JSON.stringify(InsuranceSellingPlanID)
                        );
                    } else {
                        if (!$("#add-insurance").hasClass("active")) {
                            sessionStorage.removeItem("insurance");
                        }
                    }
                }
                if (step !== "one") {
                    $(".info-section").hide();
                } else {
                    $(".info-section").show();
                }
            }
        });
        $(".js-add-addon").click(function (e) {
            e.preventDefault();
            const layoutProdVariantId = $(this).data("prod-variant-id");
            const layoutProdId = $(this).data("prod-id");
            const mainProductId = getProductId();
            // eslint-disable-next-line no-undef
            const positionKey = getPositionKey(mainProductId);
            // eslint-disable-next-line no-undef
            const filteredAddonsByKey = addonProdVariants.filter(
                ({ sku, prodId: addonVariantId }) =>
                    layoutProdId === addonVariantId && sku.includes(positionKey)
            );
            const prodId = filteredAddonsByKey.length
                ? filteredAddonsByKey[0].id
                : layoutProdVariantId;
            let addOnIds = JSON.parse(sessionStorage.getItem("addOnProducts")) ?? [];
            // If de selected, remove from session storage.
            if ($(this).hasClass("selected")) {
                $(this).removeClass("selected");
                addOnIds = addOnIds.filter((item) => item !== prodId);
                window.sessionStorage.setItem("addOnProducts", JSON.stringify(addOnIds));
                $(this).text("Add");
                return;
            }
            // If selected, store pid in session storage.
            $(this).addClass("selected");
            $(this).text("Added");
            !addOnIds.includes(prodId) && addOnIds.push(prodId);
            window.sessionStorage.setItem("addOnProducts", JSON.stringify(addOnIds));
        });
        /* Clicking options in insurance section */
        $("#add-insurance").click(function (e) {
            if (!$("#add-insurance").hasClass("tick-box")) {
                $("#add-insurance").replaceClass("white-box tick-box");
                $("#no-insurance").replaceClass("tick-box white-box");
            }
        });
        $("#no-insurance").click(function (e) {
            if (!$("#no-insurance").hasClass("tick-box")) {
                $("#no-insurance").replaceClass("white-box tick-box");
                $("#add-insurance").replaceClass("tick-box white-box");
            }
        });
        (function ($) {
            $.fn.replaceClass = function (classes) {
                const allClasses = classes.split(/\s+/).slice(0, 2);
                return this.each(function () {
                    $(this).toggleClass(allClasses.join(" "));
                });
            };
        })(jQuery);
        $(".js-pdp-pay-price").html($("#ProductPrice").html());
        setTimeout(function () {
            $(".js-pdp-pay-price").html($(".rc_widget__price--onetime").html());
            /* Pay - Subscription - then hide the "no insurance" as insurance is always added with subscription */
            const addInsurance = $("#add-insurance").children("h4").html();
            $(".rc_widget__option input[type=radio]").click(function (e) {
                /* note: Working updated Aug 23rd 2021, now with subscription the "no insurance" option will be visible */
                $("#no-insurance").show();
                $("#add-insurance").children("h4").html(addInsurance);
                $(".js-pdp-pay-price").html($(".rc_widget__price--onetime").html());
            });
        }, 500);
        const isItemInCart = (itemToAdd) => {
            // eslint-disable-next-line no-undef
            const cartData = CartJS.cart;
            const isCarEmpty = !cartData.item_count;
            if (isCarEmpty) {
                return null;
            }
            const cartItemsList = cartData.items;
            const productId = itemToAdd.id;
            return !!cartItemsList.filter(({ id }) => id === productId).length;
        };
        const getExistingItems = (items) =>
            items.filter((cartItem) => {
                return isItemInCart(cartItem);
            });
        const getNewItems = (items) => items.filter((cartItem) => !isItemInCart(cartItem));
        const getItemCartIndex = (itemsToAdd) => {
            // eslint-disable-next-line no-undef
            const cartItemsList = CartJS.cart.items;
            const productId = itemsToAdd.id;
            const indexArray = cartItemsList.findIndex((item) => item.variant_id === productId);
            return indexArray + 1;
        };
    });
};
