class Customer {
    constructor({ isLogged, isAnyCustomerStatus }) {
        this.isCustomerLogged = isLogged;
        this.isAnyCustomerStatus = isAnyCustomerStatus;
    }

    getCustomerStatus() {
        if (this.isAnyCustomerStatus) {
            return "anyCustomerStatus";
        }
        return this.isCustomerLogged ? "loggedIn" : "loggedOut";
    }
}

export default Customer;
