import LoggedSwitcher from "../switchersOptionsClasses/loggedRender";
import Product from "../productClass";

jest.mock("../productClass");

let switchersClass = null;

const constructorParameters = {
    updateProductUrl: jest.fn(),
    renderOptions: jest.fn(),
    renderTrialComponent: jest.fn(),
    renderOriginComponent: jest.fn(),
    updateOriginProductPage: jest.fn(),
    renderPaymentTermsInfoComponent: jest.fn(),
    renderTitleComponent: jest.fn(),
    renderPriceComponent: jest.fn(),
    productState: true,
};

beforeAll(() => {
    Product.mockClear();
    switchersClass = new LoggedSwitcher(constructorParameters);
});

window.originalProduct = {
    title: "originalProductTitle",
    variants: [
        { id: 123, price: "100", compare_at_price: null },
        { id: 1234, price: "100", compare_at_price: null },
    ],
};
window.trialProduct = {
    title: "trialProductTitle",
    variants: [
        { id: 1398, price: "100", compare_at_price: null },
        { id: 13569, price: "100", compare_at_price: null },
    ],
};
window.subscriptionProducts = [
    { id: 1398, price: "100", compare_at_price: null },
    { id: 13569, price: "100", compare_at_price: null },
];

describe("SwitchersManager Modal test add open class to clicked element", () => {
    test("SwitchersManager test default state", () => {
        expect(switchersClass.getOriginSwitcherState()).toBe(true);
        expect(switchersClass.getTrialSwitcherState()).toBe(false);
    });
    test("SwitchersManager test default state", () => {
        switchersClass.clickTrialSwitcher();
        expect(switchersClass.getOriginSwitcherState()).toBe(false);
        expect(switchersClass.getTrialSwitcherState()).toBe(true);
        switchersClass.clickOriginSwitcher();
        expect(switchersClass.getOriginSwitcherState()).toBe(true);
        expect(switchersClass.getTrialSwitcherState()).toBe(false);
        expect(switchersClass.getAddToCartBtnText()).toBe("Bite Now");
    });
});
