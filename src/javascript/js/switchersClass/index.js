import Product from "../productClass";
import ProductDAO from "../productDAO";
import { clearChosenVariantId } from "globalServices";

class SwitchersManager {
    constructor({
        productState,
        renderOptions,
        updateProductUrl,
        renderTrialComponent,
        renderOriginComponent,
        updateOriginProductPage,
        renderPaymentTermsInfoComponent,
        renderTitleComponent,
        renderPriceComponent,
    }) {
        clearChosenVariantId();
        this.isOrigin = productState;
        this.renderOptions = renderOptions;
        this.updateProductUrl = updateProductUrl;
        this.renderTrialComponent = renderTrialComponent;
        this.renderOriginComponent = renderOriginComponent;
        this.updateOriginProductPage = updateOriginProductPage;
        this.renderPaymentTermsInfoComponent = renderPaymentTermsInfoComponent;
        this.renderTitleComponent = renderTitleComponent;
        this.renderPriceComponent = renderPriceComponent;
        this.clickTrialSwitcher = this.clickTrialSwitcher.bind(this);
        this.clickOriginSwitcher = this.clickOriginSwitcher.bind(this);
        this.getTrialSwitcherState = this.getTrialSwitcherState.bind(this);
        this.getOriginSwitcherState = this.getOriginSwitcherState.bind(this);
    }

    getOriginSwitcherState() {
        return this.isOrigin;
    }

    getTrialSwitcherState() {
        return !this.isOrigin;
    }

    clickOriginSwitcher() {
        this.isOrigin = true;
        clearChosenVariantId();
        this.render(this);
    }

    clickTrialSwitcher() {
        this.isOrigin = false;
        clearChosenVariantId();
        this.render(this);
    }

    renderPrice() {
        const product = this.getProduct();
        this.renderPriceComponent(product.getVariantPrice());
    }

    getProduct() {
        const productDAO = new ProductDAO();
        const activeProduct = this.isOrigin
            ? productDAO.getOriginProduct()
            : productDAO.getTrialProduct();
        return new Product(activeProduct);
    }

    getProductFromList() {
        const productDAO = new ProductDAO();
        const activeProduct = this.isOrigin
            ? productDAO.getOriginProduct()
            : productDAO.getFirstSubscriptionProduct();
        return new Product(activeProduct);
    }

    getAddToCartBtnText() {
        return this.isOrigin ? "Bite Now" : "subscribe";
    }
}

export default SwitchersManager;
