(() => {
	document.addEventListener("DOMContentLoaded", () => {
		// <!-- DO NOT MODIFY -->
		// <!-- Quora Pixel Code (JS Helper) -->
			!(function (q, e, v, n, t, s) {
					if (q.qp) return;
					n = q.qp = function () {
							n.qp ? n.qp.apply(n, arguments) : n.queue.push(arguments);
					};
					n.queue = [];
					t = document.createElement(e);
					t.async = !0;
					t.src = v;
					s = document.getElementsByTagName(e)[0];
					s.parentNode.insertBefore(t, s);
			})(window, "script", "https://a.quora.com/qevents.js");
			// Email will be hashed by the pixel using SHA-256
			qp("init", "55bc4771955548e28fe862a7e89d78e4");
			qp("track", "ViewContent");
			window.location.href.includes("checkouts") && qp("track", "Purchase");
			/* <!-- End of Quora Pixel Code --> */
	});
})();
