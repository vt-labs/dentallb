$(document).ready(function ($) {
  /*Adding insurance box after form */
  $(".edit_checkout").children(".step__sections").append(insuranceHTML);
  /*Insurance box click storing values in session storage and redirect to insurance page  */
  $(document).on("click", ".js__add-insurance", function (e) {
    if (!$("#add-insurance").hasClass("tick-box")) {
      prodId = $("#add-insurance").data("pid");
      const InsuranceSellingPlanID = $("#add-insurance").attr(
        "data-sellingplanidInsurance"
      );
      const items = [
        {
          id:prodId,
          quantity: 1,
          selling_plan:InsuranceSellingPlanID
        }
      ]
      addItemsToCart(items)
    }
  });
  const INTERVAL_MILLISECONDS = 200
  const INTERVAL_PERIOD_MILLISECONDS = 6000
  const intervalId = setInterval(() => {
    const shippingElementWrapper = document.querySelector(".section--shipping-method")
    const subscriptionMessageWrapper = document.querySelector(".section--subscription-agreement");
    const subscription = document
      .querySelector(".radio__label__primary[data-shipping-method-label-title='Subscription shipping']")
    if (subscription && shippingElementWrapper) {
      subscription.innerHTML = "Free shipping"
    }
    if (subscriptionMessageWrapper) {
      subscriptionMessageWrapper
        .querySelector(".checkbox__label")
        .textContent =  "I understand that I will be charged a one-time payment for the remaining balance 28 days from now."
    };
    const productsInBin = document.querySelectorAll(".product__description__name");
    const shippingMessage = document
      .querySelector(".review-block__content[data-review-section='shipping-cost']");
    const shippingContent = `<strong class="emphasis skeleton-while-loading--inline">
      Free shipping</strong>`
    productsInBin.forEach((item) => {
      if (item.textContent.toLocaleLowerCase().includes("trial")) {
         shippingMessage.innerHTML = shippingContent;
      }
    })
  }, INTERVAL_MILLISECONDS)
  setTimeout(() => clearInterval(intervalId), INTERVAL_PERIOD_MILLISECONDS) ;
 
  /*Insurance box click storing values in session storage and redirect to insurance page  */
  $(document).on("click", ".js__remove-insurance", function (e) {
    if ($("#add-insurance").hasClass("tick-box")) {
      prodId = $("#add-insurance").data("pid");
      const item =
        {
          prodId,
          quantity: 0,
        }
      updateCartItems(item)
    }
  });

  /*View insurance click and popup open*/
  $(document).on("click", ".open-insurance-popup", function (e) {
    e.preventDefault();
    e.stopPropagation();
    $("#popup-info-insurance").show();
  });
  /*each item table if tain insurance then insurance box added tick box class  */
  $(".product-table tbody")
    .children("tr")
    .each(function (index) {
      var productName = $(this)
        .children(".product__description")
        .children(".product__description__name")
        .html()
        .toLowerCase();
      if (productName.indexOf("insurance") > -1) {
        $("#add-insurance").addClass("tick-box");
        $("#add-insurance").removeClass("white-box");
        $(".js__add-insurance").hide();
        $(".js__remove-insurance").show();
      }
    });
  /*Close click and popup close*/
  $(document).on("click", ".js-popup-close", function (e) {
    $("#popup-info-insurance").hide();
  });
});

const insuranceHTML = `<div class="js__insurance-data">
<div class="pdp-section">
  <div class="pdp-step-two  pdp-steps  "style="display: block;">
    <h2>Would You Like Insurance?</h2><br>
    <div class="notice hide">
      <p>
        If you are ordering for multiple people, then you can edit the quantity on the cart page.
      </p>
    </div>
    <div class="white-box" id="add-insurance" data-pid="33416238170165" data-sellingplanidinsurance="5374005">
     <div>
      <h4>Protect Against Loss, Damage and More for $39.00/year</h4>
      <div class="note-section">
        <a class="note-link open-insurance-popup" >Why Get Insurance?</a>
      </div>
    </div>
      <a class="btn-red js__add-insurance" >add</a>
      <a class="btn-red js__remove-insurance" style="display: none;" >remove</a>
    </div>
  </div>
</div>
</div>
<style>
.tick-box{
  display: flex;
  justify-content: space-between;
  align-items: center;
}
.pdp-section {
padding: 30px 0px;
}

.white-box{
display: flex;
align-items: center;
justify-content: space-between;
}

.white-box .btn-red{
margin-top: 0px !important;
flex: none;
height: 50px;
margin-left: 20px;
}

@media only screen and (max-width: 480px) {
.white-box{
flex-direction: column;
justify-content: flex-start;
align-items: flex-start;
}

.white-box .btn-red{
margin-top: 10px !important;
margin-left: 0px;
}
}
</style>`

const fetchData = (data, cartMethod)=>{
  fetch(`/cart/${cartMethod}.js`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  })
    .then(() => {
      window.location.href = "/checkout"
    })
    .catch((error) => {
      console.error("Error:", error);
    });
}

const addItemsToCart =  (items)=>{
  const formData = {
    items,
  };
  fetchData(formData, "add")
}
const updateCartItems =  ({prodId, quantity})=>{
  const formData = {
    updates: {[prodId]:quantity},
  };
  fetchData(formData, "update")
}
