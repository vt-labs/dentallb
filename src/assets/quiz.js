$(document).ready(function ($) {
    /*Quiz landing button click and show first question*/
    $(".js-btn-quiz-get-start").click(function (e) {
        $(".quiz-landing").hide();
        $(".js-quiz-one").show();
        $(".quiz-progress-bar").show();
        $(".quiz-progress-bar")
            .children("ul")
            .children("li:first-child")
            .addClass("active");
    });
    /*lis click add active class*/
    $(".js-quiz-screens ul")
        .children("li")
        .click(function (e) {
            $(this).parent("ul").children("li").removeClass("active");
            $(this).addClass("active");
        });
    var QuizProgress = 1;
    var PreviousQuestion = "";
    var PreviousQuizProgress = "";
    var NextQuestion = "";
    var QuestionOneValue = "";
    var QuizProduct = "";
    var QuizSignUpEmail = "";
    /*Question 1 Next Click*/
    $(".js-quiz-next-btn-one").click(function (e) {
        PreviousQuestion = "one";
        PreviousQuizProgress = 1;
        if ($(".js-quiz-one ul").children("li").hasClass("active")) {
            QuestionOneValue = $(".js-quiz-one ul")
                .children("li.active")
                .children(".inner")
                .children("h5")
                .html()
                .trim();
            if (QuestionOneValue == "Grinder") {
                NextQuestion = "two";
                QuizProgress = 2;
            } else if (QuestionOneValue == "Clencher") {
                NextQuestion = "three";
                QuizProgress = 3;
            } else {
                NextQuestion = "four";
                QuizProgress = 4;
            }
            $(".js-quiz-screens").hide();
            $(".js-quiz-" + NextQuestion).show();
            ProgressBarFill(QuizProgress);
        } else {
        }
    });
    var QuestionTwoValue = "";
    /*Question 2 Next Click*/
    $(".js-quiz-next-btn-two").click(function (e) {
        PreviousQuestion = "two";
        PreviousQuizProgress = 2;
        if ($(".js-quiz-two ul").children("li").hasClass("active")) {
            QuestionTwoValue = $(".js-quiz-two ul")
                .children("li.active")
                .children(".inner")
                .children("h5")
                .html()
                .trim();
            NextQuestion = "four";
            QuizProgress = 4;
            $(".js-quiz-screens").hide();
            $(".js-quiz-" + NextQuestion).show();
            ProgressBarFill(QuizProgress);
        } else {
        }
    });
    var QuestionThreeValue = "";
    /*Question 3 Next Click*/
    $(".js-quiz-next-btn-three").click(function (e) {
        PreviousQuestion = "three";
        PreviousQuizProgress = 3;
        if ($(".js-quiz-three ul").children("li").hasClass("active")) {
            QuestionThreeValue = $(".js-quiz-three ul")
                .children("li.active")
                .children(".inner")
                .children("h5")
                .html()
                .trim();
            NextQuestion = "four";
            QuizProgress = 4;
            $(".js-quiz-screens").hide();
            $(".js-quiz-" + NextQuestion).show();
            ProgressBarFill(QuizProgress);
        } else {
        }
    });
    var QuestionFourValue = "";
    /*Question 4 Next Click*/
    $(".js-quiz-next-btn-four").click(function (e) {
        PreviousQuestion = "four";
        PreviousQuizProgress = 4;
        if ($(".js-quiz-four ul").children("li").hasClass("active")) {
            QuestionFourValue = $(".js-quiz-four ul")
                .children("li.active")
                .children(".inner")
                .children("h5")
                .html()
                .trim();
            NextQuestion = "five";
            QuizProgress = 5;
            $(".js-quiz-screens").hide();
            $(".js-quiz-" + NextQuestion).show();
            ProgressBarFill(QuizProgress);
        } else {
        }
    });
    var QuestionFiveValue = "";
    /*Question 5 Next Click*/
    $(".js-quiz-next-btn-five").click(function (e) {
        PreviousQuestion = "five";
        PreviousQuizProgress = 5;
        if ($(".js-quiz-five ul").children("li").hasClass("active")) {
            QuestionFiveValue = $(".js-quiz-five ul")
                .children("li.active")
                .children(".inner")
                .children("h5")
                .html()
                .trim();
            if (QuestionFiveValue == "Yes") {
                QuizProduct = "Soft Guard";
                NextQuestion = "success";
            } else {
                NextQuestion = "six";
                QuizProgress = 6;
            }
            $(".js-quiz-screens").hide();
            $(".js-quiz-" + NextQuestion).show();
            ProgressBarFill(QuizProgress);
        } else {
        }
    });
    var QuestionSixValue = "";
    /*Question 6 Next Click*/
    $(".js-quiz-next-btn-six").click(function (e) {
        PreviousQuestion = "six";
        PreviousQuizProgress = 6;
        if ($(".js-quiz-six ul").children("li").hasClass("active")) {
            QuestionSixValue = $(".js-quiz-six ul")
                .children("li.active")
                .children(".inner")
                .children("h5")
                .html()
                .trim();
            if (QuestionSixValue == "Yes, I have an irregular bite") {
                QuizProduct = "3D Upgrade";
                NextQuestion = "success";
            } else {
                NextQuestion = "seven";
                QuizProgress = 7;
            }
            $(".js-quiz-screens").hide();
            $(".js-quiz-" + NextQuestion).show();
            ProgressBarFill(QuizProgress);
        } else {
        }
    });
    var QuestionSevenValue = "";
    /*Question 7 Next Click*/
    $(".js-quiz-next-btn-seven").click(function (e) {
        PreviousQuestion = "seven";
        PreviousQuizProgress = 7;
        NextQuestion = "success";
        if ($(".js-quiz-seven ul").children("li").hasClass("active")) {
            QuestionSevenValue = $(".js-quiz-seven ul")
                .children("li.active")
                .children(".inner")
                .children("h5")
                .html()
                .trim();
            if (QuestionSevenValue == "Yes" && QuestionOneValue == "I Don’t Know") {
                QuizProduct = "Hybird Day";
            } else if (
                QuestionSevenValue == "No" &&
                QuestionOneValue == "I Don’t Know"
            ) {
                QuizProduct = "Hybird";
            } else if (
                QuestionSevenValue == "Yes" &&
                QuestionTwoValue == "I'm a light grinder" &&
                QuestionFourValue == "Yes"
            ) {
                QuizProduct = "Hybird Day";
            } else if (
                QuestionSevenValue == "Yes" &&
                QuestionThreeValue == "I'm a light clencher" &&
                QuestionFourValue == "Yes"
            ) {
                QuizProduct = "Durable Day";
            } else if (
                QuestionSevenValue == "Yes" &&
                QuestionTwoValue == "I'm a moderate grinder" &&
                QuestionFourValue == "No"
            ) {
                QuizProduct = "Hybird Day";
            } else if (
                QuestionSevenValue == "Yes" &&
                QuestionTwoValue == "I’m a strong grinder" &&
                QuestionFourValue == "No"
            ) {
                QuizProduct = "Extra Durable Day";
            } else if (
                QuestionSevenValue == "Yes" &&
                QuestionThreeValue == "I'm a moderate clencher" &&
                QuestionFourValue == "No"
            ) {
                QuizProduct = "Extra Durable Day";
            } else if (
                QuestionSevenValue == "Yes" &&
                QuestionThreeValue == "I’m a strong clencher" &&
                QuestionFourValue == "No"
            ) {
                QuizProduct = "Durable Day";
            } else if (
                QuestionSevenValue == "No" &&
                QuestionTwoValue == "I'm a moderate grinder" &&
                QuestionFourValue == "No"
            ) {
                QuizProduct = "Hybird";
            } else if (
                QuestionSevenValue == "No" &&
                QuestionTwoValue == "I’m a strong grinder" &&
                QuestionFourValue == "No"
            ) {
                QuizProduct = "Extra Durable";
            } else if (
                QuestionSevenValue == "No" &&
                QuestionThreeValue == "I'm a moderate clencher" &&
                QuestionFourValue == "No"
            ) {
                QuizProduct = "Extra Durable";
            } else if (
                QuestionSevenValue == "No" &&
                QuestionThreeValue == "I’m a strong clencher" &&
                QuestionFourValue == "No"
            ) {
                QuizProduct = "Durable Guard";
            } else if (
                QuestionSevenValue == "No" &&
                QuestionTwoValue == "I'm a light grinder"
            ) {
                QuizProduct = "Day Guard";
            } else if (
                QuestionSevenValue == "No" &&
                QuestionThreeValue == "I'm a light clencher"
            ) {
                QuizProduct = "Day Guard";
            } else if (
                QuestionSevenValue == "Yes" &&
                QuestionThreeValue == "I'm a light clencher"
            ) {
                QuizProduct = "Day Guard";
            } else if (
                QuestionSevenValue == "Yes" &&
                QuestionTwoValue == "I'm a light grinder"
            ) {
                QuizProduct = "Day Guard";
            } else {
                QuizProduct = "Hybird";
            }
            $(".js-quiz-screens").hide();
            $(".js-quiz-" + NextQuestion).show();
            ProgressBarFill(QuizProgress);
        } else {
        }
    });
    /*Quiz Question prev click*/
    $(".js-quiz-prev-btn").click(function (e) {
        $(".js-quiz-screens").hide();
        var buttonValue = $(this).attr("data-attr-quiz");
        if (buttonValue == "two") {
            $(".js-quiz-one").show();
        }
        if (buttonValue == "three") {
            $(".js-quiz-one").show();
        }
        if (buttonValue == "four") {
            if (QuestionThreeValue != "") {
                $(".js-quiz-three").show();
            } else if (QuestionTwoValue != "") {
                $(".js-quiz-two").show();
            } else {
                $(".js-quiz-one").show();
            }
        }
        if (buttonValue == "five") {
            $(".js-quiz-four").show();
        }
        if (buttonValue == "six") {
            $(".js-quiz-five").show();
        }
        if (buttonValue == "seven") {
            $(".js-quiz-six").show();
        }
        ProgressBarFill(PreviousQuizProgress);
    });
    /*ProgressBar */
    function ProgressBarFill(value) {
        $(".quiz-progress-bar").children("ul").children("li").removeClass("active");
        for (var i = 1; i <= value; i++) {
            $(".quiz-progress-bar")
                .children("ul")
                .children("li:nth-child(" + i + ")")
                .addClass("active");
        }
    }
    /*Show Product Screen */
    function ProductScreenVisible() {
        $(".js-quiz-eight-subheading").hide();
        $(".js-quiz-eight ul").children("li").hide();
        var countProduct = 0;
        $(".js-quiz-eight li").each(function (index) {
            //var Product = $(this).children(".inner").children("h5").html().trim();
            var Product = $(this).attr("data-product").trim();
            if (Product == QuizProduct) {
                ProgressBarFill(8);
                $(this).show();
                $(".js-quiz-screens").hide();
                $(".js-quiz-eight").show();
                countProduct++;
            }
        });
        if (countProduct > 1) {
            $(".js-quiz-eight-subheading").html(
                "One for nighttime use; one for daytime!"
            );
            $(".js-quiz-eight-subheading").show();
        }
    }
    /*Email Validate*/
    function validateEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    /*Quiz Success Submit click*/
    $("#quiz-signup-submit").click(function (e) {
        var QuizSignUpEmail = $("#text-quiz-signup-email").val();
        if (QuizSignUpEmail != "") {
            if (validateEmail(QuizSignUpEmail)) {
                ProductScreenVisible();
            } else {
            }
        }
    });
    /*Quiz Success Skip click*/
    $(".js-quiz-signup-skip").click(function (e) {
        ProductScreenVisible();
    });
    /*Quiz Question next click*/
    /* $(".js-quiz-next-btn").click(function(e) {
           var quizValue = $(this).attr("data-attr-quiz");
           $(".js-quiz-screens").hide();
           $(".js-quiz-" + quizValue).show();
           QuizProgress++;
           $(".quiz-progress-bar")
               .children("ul")
               .children("li:nth-child(" + QuizProgress + ")")
               .addClass("active");
       });*/
    /*Quiz Question prev click*/
    /* $(".js-quiz-prev-btn").click(function(e) {
           var quizValue = $(this).attr("data-attr-quiz");
           $(".js-quiz-screens").hide();
           $(".js-quiz-" + quizValue).show();
           $(".quiz-progress-bar")
               .children("ul")
               .children("li:nth-child(" + QuizProgress + ")")
               .removeClass("active");
           QuizProgress--;
       });*/
    /* save the product variant ID
       So that we know they are coming from the quiz page and if 
       they add to cart - then the line item property quiz: yes would be added
       against the product
       */
    $(".js-add-line-item-property-for-quiz").click(function (e) {
        e.preventDefault();
        let variantID = parseInt($(this).attr("data-attr-itemid"));
        localStorage.setItem("quizitem", variantID);
        window.location.replace($(this).attr("href"));
    });
});