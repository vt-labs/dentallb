$(document).ready(function ($) {
  //     setInterval(updateLineItemProperties, 1000);
  addonCheck();

  /* Popup */
  $(".open-popup").magnificPopup({
    type: "inline",

    // Fixed position will be used
    fixContentPos: true,

    // Since disabled, Magnific Popup
    // will not put close button
    // inside content of popup
    closeBtnInside: false,
    preloader: false,

    // Delay in milliseconds before
    // popup is removed
    removalDelay: 160,

    // Class that is added to
    // popup wrapper and background
    mainClass: "mfp-fade",
  });

  $(".mfp-popup-close").click(function (e) {
    $.magnificPopup.close();
  });

  /* Rating Slick SLider */
  jQuery(".customer-ratings").slick({
    dots: true,
    arrows: true,
    infinite: true,
    speed: 500,
    draggable: false,
    cssEase: "ease-in-out",
    autoplay: true,
    autoplaySpeed: 3000,
    prevArrow: '<div class="slick-prev"></div>',
    nextArrow: '<div class="slick-next"></div>',
    responsive: [
      {
        breakpoint: 900,
        settings: {
          arrows: false,
        },
      },
    ],
  });
  /* homepage Phase 2 - Logo Slick SLider */
  jQuery(".js-logo-slider").slick({
    dots: false,
    arrows: false,
    infinite: false,
    speed: 500,
    draggable: true,
    cssEase: "ease-in-out",
    autoplay: false,
    autoplaySpeed: 3000,
    prevArrow: '<div class="slick-prev"></div>',
    nextArrow: '<div class="slick-next"></div>',
    slidesToShow: 5,
    variableWidth: true,
    responsive: [
      {
        breakpoint: 980,
        settings: {
          dots: true,
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 768,
        settings: {
          autoplay: true,
          dots: true,
          slidesToShow: 2,
          infinite: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          autoplay: true,
          infinite: true,
          dots: true,
          variableWidth: false,
        },
      },
    ],
  });

  /* Testimonial Slick SLider */
  jQuery(".testimonial-slider").slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    draggable: true,
    cssEase: "ease-in-out",
    autoplay: false,
    autoplaySpeed: 3000,
    prevArrow: '<div class="slick-prev"></div>',
    nextArrow: '<div class="slick-next"></div>',
    slidesToShow: 4,
    variableWidth: true,
    responsive: [
      {
        breakpoint: 485,
        settings: {
          arrows: false,
          dots: true,
          slidesToShow: 1,
          variableWidth: false,
        },
      },
    ],
  });
  /* active link while submenu open */
  $(".has-big-nav .site-nav__link,.submenu").mouseover(function () {
    $(".has-big-nav .site-nav__link").css("border-bottom", "2px solid #f07826");
  });
  $(".has-big-nav .site-nav__link,.submenu").mouseout(function () {
    $(".has-big-nav .site-nav__link").css("border-bottom", "none");
  });
  /* Header Search Open Close  */
  $("#search-expand").on("click", function () {
    $("#SearchDrawer").addClass("active");
  });
  $(".search-bar__close").on("click", function () {
    $("#SearchDrawer").removeClass("active");
  });

  /* Announcement Close  */
  $("#announcement-close").on("click", function () {
    $(".announcement-bar").hide();
  });

  /* Increment Counter by 1  */
  var count = 1;
  $(".half-img-half-content-cta .flex-align-center").each(function (index) {
    if (count <= 9)
      $(this)
        .find("span.count")
        .html("0" + count);
    else $(this).find("span.count").html(count);
    count++;
  });

  /* Accordion JS */
  $(".accordion-toggle").on("click", function () {
    if ($(this).hasClass("active")) {
      $(this).removeClass("active");
      $(this).siblings(".accordion-content").slideUp(200);
    } else {
      $(".accordion-toggle").removeClass("active");
      $(this).addClass("active");
      $(".accordion-content").slideUp(200);
      $(this).siblings(".accordion-content").slideDown(200);
    }
  });

  // update line item properties if it's the main product and doesn't has return customer property;
  updateLineItemProperties();

});

/** Mobile SubMenu Open Close **/
(function ($) {
  $(function () {
    // If a link has a dropdown, add sub menu toggle.
    $(".has-big-nav .site-nav__link").click(function (e) {
      $(this).toggleClass("active");
      $(this).siblings(".submenu").toggleClass("active");
      e.stopPropagation();
    });
  });
})(jQuery);

/** Mobile Navigation Open Close **/
function nav_open() {
  $("#navbarNavDropdown").toggleClass("active");
  $("#hamburger").toggleClass("active");
}

var navOffset = $('.js-header-wrap').offset().top;

/** Fix Header on Scroll **/
$(document).on("scroll", function() {
  const mainPage = document.querySelector(".page-container");
  var bodyTop = mainPage.getBoundingClientRect().top;
  $(".js-header-wrap").toggleClass("scrolled", (bodyTop < navOffset));
}); 

/* Returning Customer/Product added from the "Addon" collection page */
function addonCheck() {
  var checkAddonURLIncludeToken = getUrlParameter("token");
  console.log("checkAddonURLIncludeToken: " + checkAddonURLIncludeToken);
  if (
    checkAddonURLIncludeToken != null &&
    checkAddonURLIncludeToken != undefined &&
    checkAddonURLIncludeToken != ""
  ) {

    //create cookies and expire the next day
    let days = 1;
    var date, expires;
    if (days) {
      date = new Date();
      date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
      expires = "; expires=" + date.toUTCString();
    } else {
      expires = "";
    }
    //addonCookie=true; expires=Tue, 16 Mar 2021 20:19:51 GMT
    document.cookie = "addonCookie=" + true + expires;
    var cookieVal = getCookie("addonCookie"); // returns "turkey"
  }
}
var getCookie = function (name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
};

function getUrlParameter(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
  var results = regex.exec(location.search);
  return results === null
    ? ""
    : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function updateLineItemProperties() {
  if (localStorage.checkReturningCustomer) {
    $(CartJS.cart.items).each(function () {
      let productID = this.id;
      let quantity = this.quantity;
      let checkIfMainProduct = "_main-product" in this.properties;
      if (checkIfMainProduct) {
        let checkReturnCustomerLineItemExists =
          "Return Customer" in this.properties;
        let properties = {};
        properties = this.properties;
        if (checkReturnCustomerLineItemExists == false) {
          // update the properties object;
          properties["Return Customer"] = "Yes";
          //remove the item
          CartJS.removeItemById(productID, {
            success: function (data, textStatus, jqXHR) {},
            error: function (jqXHR, textStatus, errorThrown) {
              console.log("Error: " + errorThrown + "!");
            },
          });
          //update item line property
          CartJS.addItem(productID, 1, properties, {
            success: function (data, textStatus, jqXHR) {
              console.log("success");
            },
            error: function (jqXHR, textStatus, errorThrown) {
              console.log("Error: " + errorThrown + "!");
            },
          });
        }
      }
    });
  }
}
