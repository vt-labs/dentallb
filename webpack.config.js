const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");
const fs = require("fs");

const getFilePath = (context, absoluteFilename, extension) =>
    absoluteFilename.replace(context, "").replace(extension, "");

const removeHtmlExtension = (absoluteFilename) => absoluteFilename.replace(".html", "");

const getLiquidUrl = (context, absoluteFilename, extension) =>
    `../${getFilePath(context, absoluteFilename, extension)}${extension}`;

const getSectionFilename = (relativeFilepath) => {
    const filepathParts = relativeFilepath.split("/");
    return filepathParts[filepathParts.length - 1];
};

const getSectionUrl = (context, absoluteFilename) => {
    const sectionFileName = getSectionFilename(absoluteFilename);
    return `../sections/${getFilePath(context, sectionFileName, ".html")}`;
};

const mergeContentFiles = (content, absoluteFilename, targetFolder) => {
    const contentTags = {
        css: ["<style>", "</style>"],
        js: ["<script>", "</script>"],
        "liquid.css": ["{% stylesheet %}", "{% endstylesheet %}"],
        "liquid.js": ["{% javascript %}", "{% endjavascript %}"],
        json: ["{% schema %}", "{% endschema %}"],
    };
    const sectionFileName = getSectionFilename(absoluteFilename);
    const fileName = getFilePath("", sectionFileName, ".liquid.html");
    let stringifiedContent = content.toString();
    Object.keys(contentTags).forEach((extension) => {
        const contentTag = contentTags[extension];
        try {
            const subContent = fs.readFileSync(
                `./src/${targetFolder}/${fileName}/${fileName}.${extension}`
            );
            stringifiedContent = stringifiedContent.concat(
                `\n${contentTag[0]}\n${subContent.toString()}\n${contentTag[1]}\n`
            );
        } catch (e) {}
    });
    return stringifiedContent;
};

module.exports = {
    output: {
        filename: "bundle.js",
        path: path.resolve("./build/assets/"),
        publicPath: "",
        clean: true,
    },
    entry: "./src/assets/theme-dev.js",
    devtool: "inline-source-map",
    resolve: {
        extensions: [".tsx", ".ts", ".js"],
        alias: {
            Ui: path.resolve(__dirname, "src/javascript/js/reactProductPage/ui"),
            globalServices$: path.resolve(__dirname, "src/javascript/js/services/index.js"),
            globalUtils$: path.resolve(__dirname, "src/javascript/js/utils/index.js"),
        },
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: "ts-loader",
                exclude: /node_modules/,
            },
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: ["babel-loader"],
            },
            {
                test: /\.(scss|css)$/,
                use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
            },
            {
                test: /\.(png|svg|jpg|jpeg|gif)$/i,
                type: "asset/inline",
                issuer: {
                    not: [/\.(scss|css)$/],
                },
            },
            {
                test: /\.(png|svg|jpg|jpeg|gif)$/i,
                type: "asset",
                issuer: /\.(scss|css)$/,
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/i,
                type: "asset/resource",
            },
            {
                test: /\.html$/i,
                loader: "html-loader",
            },
        ],
    },
    plugins: [
        new CopyPlugin({
            patterns: [
                {
                    from: "./**/*(config|locales|templates)/*.json",
                    context: "./src",
                    to: "../[path][name][ext]",
                    globOptions: {
                        ignore: ["./**/sections/*.json"],
                    },
                    force: true,
                    noErrorOnMissing: true,
                },
                {
                    from: "./**/*(snippets|layout|templates|templates/customers)/*.html",
                    context: "./src",
                    to: ({ context, absoluteFilename }) =>
                        getLiquidUrl(context, removeHtmlExtension(absoluteFilename), ".liquid"),
                    force: true,
                    globOptions: {
                        ignore: [
                            "./**/*.js.liquid",
                            "./**/*.css.liquid",
                            "./**/*.scss.liquid",
                            "./**/*.svg.liquid",
                            "./**/sections/*.liquid",
                        ],
                    },
                    noErrorOnMissing: true,
                },
                {
                    from: "./**/assets/*.liquid.*",
                    context: "./src",
                    to: ({ context, absoluteFilename }) =>
                        getLiquidUrl(context, absoluteFilename, ".liquid"),
                    force: true,
                    noErrorOnMissing: true,
                },
                {
                    from: "./**/*",
                    context: "./src/assets",
                    globOptions: {
                        ignore: ["./**/*.liquid.*", "./**/bundle.js"],
                    },
                    force: true,
                    noErrorOnMissing: true,
                },
                {
                    from: "./**/*.liquid.*",
                    context: "./src/sections",
                    globOptions: {
                        ignore: ["./**/*.liquid.js", "./**/*.liquid.css", "./**/*.liquid.scss"],
                    },
                    to: ({ context, absoluteFilename }) =>
                        getSectionUrl(context, removeHtmlExtension(absoluteFilename)),
                    force: true,
                    transform: (content, absoluteFilename) =>
                        mergeContentFiles(content, absoluteFilename, "sections"),
                    noErrorOnMissing: true,
                },
            ],
        }),
        new MiniCssExtractPlugin({
            filename: "bundle.css",
        }),
    ],
};
