// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)
const { lighthouse, pa11y, prepareAudit } = require("cypress-audit");
require("dotenv").config();

module.exports = (on, config) => {
    // `on` is used to hook into various events Cypress emits
    // `config` is the resolved Cypress config

    on("before:browser:launch", (browser = {}, launchOptions) => {
        prepareAudit(launchOptions);
    });

    on("task", {
        lighthouse: lighthouse(), // calling the function is important
        pa11y: pa11y(), // calling the function is important
    });

    config.env.THEMEKIT_STORE = process.env.THEMEKIT_STORE;

    config.env.THEMEKIT_THEME_ID = process.env.THEMEKIT_THEME_ID;

    config.env.STOREFRONT_DIGEST = process.env.STOREFRONT_DIGEST;

    config.baseUrl = "https://" + config.env.THEMEKIT_STORE;

    return config;
};
