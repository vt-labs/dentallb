// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
import "cypress-audit/commands";

Cypress.Commands.overwrite("visit", (originalFn, url, options) => {
    const full_path = `${url}?preview_theme_id=${Cypress.env("THEMEKIT_THEME_ID")}`;
    if (Cypress.env("STOREFRONT_DIGEST")) {
        return cy
            .setCookie("storefront_digest", Cypress.env("STOREFRONT_DIGEST"))
            .then({ timeout: 60000 }, () => originalFn(full_path, options));
    } else {
        return originalFn(full_path, options);
    }
});
