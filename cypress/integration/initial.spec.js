describe("Store", function () {
    it("is live", function () {
        cy.visit("/");
        cy.get("body").should("exist");
        cy.lighthouse();
        cy.percySnapshot();
    });
});
