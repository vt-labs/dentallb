module.exports = {
    trailingComma: "es5",
    semi: true,
    singleQuote: false,
    arrowParens: "always",
    tabWidth: 4,
    printWidth: 100,
    insertFinalNewline: true,
};
