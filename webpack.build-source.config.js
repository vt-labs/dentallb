const fs = require("fs");
const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");
const RemoveEmptyScriptsPlugin = require("webpack-remove-empty-scripts");

const getFilePath = (context, absoluteFilename, extension) =>
    absoluteFilename.replace(context, "").replace(extension, "");

const getLiquidUrl = (context, absoluteFilename, extension) =>
    `../src/${getFilePath(context, absoluteFilename, extension)}${extension}`;

const getSectionUrl = (context, absoluteFilename, extension) =>
    `../src/sections/[name]/${getFilePath(context, absoluteFilename, extension)}${extension}`;

const getFileName = (absoluteFilename) => {
    const pathParts = absoluteFilename.split("/");
    const filenameWithExtension = pathParts[pathParts.length - 1];
    return filenameWithExtension.split(".")[0];
};

const getSubContent = ({
    content,
    firstDelimiter,
    lastDelimiter,
    alternativeFirstDelimiter,
    alternativeLastDelimiter,
}) => {
    const firstDelimiterIndex = Math.max(
        content.lastIndexOf(firstDelimiter),
        content.lastIndexOf(alternativeFirstDelimiter)
    );
    const lastDelimiterIndex = Math.max(
        content.lastIndexOf(lastDelimiter),
        content.lastIndexOf(alternativeLastDelimiter)
    );

    const NON_EXISTING_INDEX = -1;

    if (firstDelimiterIndex > NON_EXISTING_INDEX && lastDelimiterIndex > NON_EXISTING_INDEX) {
        return content.substring(firstDelimiterIndex + firstDelimiter.length, lastDelimiterIndex);
    }
    return "";
};

const writeContentFiles = (
    { schema, styleSheet, javaScript, liquidJavaScript, liquidStyleSheet },
    absoluteFilename,
    targetFolder
) => {
    const fileName = getFileName(absoluteFilename);
    const relativeFolderPath = `./src/${targetFolder}/${fileName}`;
    const relativeFilePath = `${relativeFolderPath}/${fileName}`;

    fs.mkdir(relativeFolderPath, { recursive: true }, (createFolderError) => {
        if (createFolderError) throw createFolderError;
        if (schema) {
            fs.writeFile(`${relativeFilePath}.json`, schema, (createFileError) => {
                if (createFileError) return console.log(createFileError);
            });
        }
        if (styleSheet) {
            fs.writeFile(`${relativeFilePath}.css`, styleSheet, (createFileError) => {
                if (createFileError) return console.log(createFileError);
            });
        }
        if (javaScript) {
            fs.writeFile(`${relativeFilePath}.js`, javaScript, (createFileError) => {
                if (createFileError) return console.log(createFileError);
            });
        }
        if (liquidJavaScript) {
            fs.writeFile(`${relativeFilePath}.liquid.js`, liquidJavaScript, (createFileError) => {
                if (createFileError) return console.log(createFileError);
            });
        }
        if (liquidStyleSheet) {
            fs.writeFile(`${relativeFilePath}.liquid.css`, liquidStyleSheet, (createFileError) => {
                if (createFileError) return console.log(createFileError);
            });
        }
    });
};

const separateLiquidMarkup = (content) => {
    const schema = getSubContent({
        content,
        firstDelimiter: "{% schema %}",
        lastDelimiter: "{% endschema %}",
        alternativeFirstDelimiter: "{%schema%}",
        alternativeLastDelimiter: "{%endschema%}",
    });
    const liquidStyleSheet = getSubContent({
        content,
        firstDelimiter: "{% stylesheet %}",
        lastDelimiter: "{% endstylesheet %}",
        alternativeFirstDelimiter: "{%stylesheet%}",
        alternativeLastDelimiter: "{%endstylesheet%}",
    });
    const liquidJavaScript = getSubContent({
        content,
        firstDelimiter: "{% javascript %}",
        lastDelimiter: "{% endjavascript %}",
        alternativeFirstDelimiter: "{%javascript%}",
        alternativeLastDelimiter: "{%endjavascript%}",
    });
    const javaScript = "";
    const styleSheet = getSubContent({
        content,
        firstDelimiter: "<style>",
        lastDelimiter: "</style>",
        alternativeFirstDelimiter: "<style >",
        alternativeLastDelimiter: "</ style>",
    });

    const html = content
        .replace(schema, "")
        .replace(liquidStyleSheet, "")
        .replace(liquidJavaScript, "")
        .replace("{% schema %}", "")
        .replace("{% endschema %}", "")
        .replace("{% stylesheet %}", "")
        .replace("{% endstylesheet %}", "")
        .replace("{% javascript %}", "")
        .replace("{% endjavascript %}", "");

    return {
        html,
        schema,
        liquidJavaScript,
        liquidStyleSheet,
        javaScript,
        styleSheet,
    };
};

module.exports = {
    mode: "development",
    output: {
        path: path.resolve(__dirname, "src"),
    },
    entry: {},
    plugins: [
        new CopyPlugin({
            patterns: [
                {
                    from: "./**/*.liquid",
                    context: "./build",
                    to: ({ context, absoluteFilename }) =>
                        getLiquidUrl(context, absoluteFilename, ".html"),
                    force: true,
                    globOptions: {
                        ignore: [
                            "./**/*.js.liquid",
                            "./**/*.css.liquid",
                            "./**/*.scss.liquid",
                            "./**/*.svg.liquid",
                            "./**/sections/*.liquid",
                        ],
                    },
                    noErrorOnMissing: true,
                },
                {
                    from: "./**/*.liquid",
                    context: "./build/sections",
                    to: ({ context, absoluteFilename }) =>
                        `${getSectionUrl(context, absoluteFilename, ".html")}`,
                    force: true,
                    transform(content, absoluteFilename) {
                        const separatedContent = separateLiquidMarkup(content.toString());
                        writeContentFiles(separatedContent, absoluteFilename, "sections");
                        return separatedContent.html;
                    },
                    noErrorOnMissing: true,
                },
                {
                    from: "./**/*.js.liquid",
                    context: "./build",
                    to: ({ context, absoluteFilename }) =>
                        getLiquidUrl(context, absoluteFilename, ".js"),
                    force: true,
                    noErrorOnMissing: true,
                },
                {
                    from: "./**/*.css.liquid",
                    context: "./build",
                    to: ({ context, absoluteFilename }) =>
                        getLiquidUrl(context, absoluteFilename, ".css"),
                    force: true,
                    noErrorOnMissing: true,
                },
                {
                    from: "./**/*.scss.liquid",
                    context: "./build",
                    to: ({ context, absoluteFilename }) =>
                        getLiquidUrl(context, absoluteFilename, ".scss"),
                    force: true,
                    noErrorOnMissing: true,
                },
                {
                    from: "./**/*.svg.liquid",
                    context: "./build",
                    to: ({ context, absoluteFilename }) =>
                        getLiquidUrl(context, absoluteFilename, ".svg"),
                    force: true,
                    noErrorOnMissing: true,
                },
                {
                    from: "./**/*",
                    context: "./build/assets",
                    to: "assets/[path][name][ext]",
                    globOptions: {
                        ignore: ["./**/*.liquid", "./**/bundle.js"],
                    },
                    force: true,
                    noErrorOnMissing: true,
                },
                {
                    from: "./**/*.json",
                    context: "./build",
                    force: true,
                    noErrorOnMissing: true,
                },
            ],
        }),
        new RemoveEmptyScriptsPlugin(),
    ],
};
