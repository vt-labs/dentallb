# Shopify Webpack Integration

## List of tools

-   TypeScript
-   ThemeKit
-   Webpack
-   ESLint
-   Prettier
-   Jest
-   Cypress
-   SonarCloud
-   LiquidJS

## Installation

This setup works with any theme.

1. Prepare `sonar-project.properties` file

```
sonar.projectKey=The project's unique key.
```

2. Prepare `.env` file

```
THEMEKIT_PASSWORD=12345
THEMEKIT_THEME_ID=12345
THEMEKIT_STORE=store.myshopify.com
```

3. Run `npm run theme:setup` to import existing theme

4. OR run

```
npm run build
npm run theme:deploy
```

to work with a skeleton theme

## Development

-   `npm run dev`
-   Input js file is `src/assets/theme.js`
-   Output js file is `build/assets/bundle.js`
-   Output css file is `build/assets/bundle.css`
-   Update `theme.liquid.html` file with following

```
  +{{ 'bundle.css' | asset_url | stylesheet_tag }}
  +<script src="{{ 'bundle.js' | asset_url }}" defer="defer"></script>
  -<script src="{{ 'theme.js' | asset_url }}" defer="defer"></script>
```

-   Use `/javascript` folder to develop new code and import in in `theme.js`
-   Use TypeScript instead of JavaScript in `/javascript` folder
-   Styles/Images/Fonts can be imported in any `.js` or `.ts` file.
-   Images imported in `.js` or `.ts` files are transformed in base64 format
-   Images imported in `.css` or `.scss` files are transformed in base64 format im they are larger than 8kb, otherwise they are copied to build asset folder

## Filename transformation

-   `bundle/assets/asset.js.liquid` -> `src/assets/asset.liquid.js`
-   `bundle/snippets/snippet.liquid` -> `src/snippets/snippet.liquid.html`

## Sections

1. Each section is separated into different files
2. `.json` is for `{% schema %}`
3. `.liquid.js` is for `{% javascript %}`
4. `.liquid.css` is for `{% stylesheet %}`
5. `.css` is for `<style>`
6. `.js` is for `<javascript>`
7. All this files can contain Liquid tags

## Unit Testing

1. Use Jest for unit tests.
2. Test liquid files

```
import section from "./example-section.html";
import { Liquid } from "liquidjs";

const engine = new Liquid();

describe("example section", () => {
    it("should render shop name", async () => {
        const renderedSection: string = await engine.parseAndRender(section, {
            shop: { name: "Example shop" },
        });
        const html: HTMLDivElement = document.createElement("div");
        html.innerHTML = renderedSection;
        expect(html.textContent).toContain("Example shop");
    });
});

```

## E2E Testing

1. Use Cypress for E2E testing

```
describe("Store", function () {
    it("is live", function () {
        cy.visit("/");
        cy.get("body").should("exist");
    });
});

```

2. Add `STOREFRONT_DIGEST` ENV variable if the store is password-protected. Find the value in `storefront_digest` cookie.
3. Add `CYPRESS_PROJECT_ID`, `CYPRESS_RECORD_KEY` and `PERCY_TOKEN` to Bitbucket ENV variables. Do not add them locally.
4. Use command `cy.lighthouse()` to perform Google Page Speed audit.
5. Default config for Google Lighthouse in `cypress.json`
6. Use command `cy.pa11y()` to perform accessibility audit.
7. https://bitbucket.org/vt-labs/shopify-theme-skeleton/pull-requests/13 - lighthouse implementation
